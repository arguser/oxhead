<div align="center">

<img src="https://gitlab.com/arguser/oxhead/raw/master/src/assets/oxhead.png" width="150" />

# Ox Head


Intents to be a lightweight dashboard and control system for your [Jörmungandr](http://github.com/input-output-hk/jormungandr) Cardano node.

</div>


## Mitology

Thor catch Jörmungandr by preparing a strong line and a large hook which baits it with an **Ox Head**.

## Examples

### Node

### Stake Distribution

### Stake Pool Leaders

## Requirements

* Running [Jörmungandr](http://github.com/input-output-hk/jormungandr) Node
* Have the REST address reachable from your network
* [Enable CORS](https://input-output-hk.github.io/jormungandr/configuration/network.html?highlight=cors#rest-interface-configuration) for this domain in node config


## Development

### Requirements

* [Elm](http://elm-lang.org/)
* Running [Jörmungandr](http://github.com/input-output-hk/jormungandr) Node
* Have the REST address reachable from your network

```
elm make src/OxHead.elm --output elm.js --debug
elm reactor
http://localhost:8000/index.html
```

See jormungandr's [documentation](https://input-output-hk.github.io/jormungandr/quickstart/03_rest_api.html).

### Feedback

Suggestions are welcome at the [Issue Board](https://gitlab.com/arguser/oxhead/-/boards)
