# Changelog

## [0.30.0] - 2020-06-26

### Added

- Node IP Address from Network Stats

### Changed

- Network Stats List to Table

### Removed

- Node ID from Network Stats

## [0.29.0] - 2020-06-17

### Added

- Improved visibility on table rows

### Changed

- Leaders logs tables visible if not empty
- Main font set to Teko
- Commented all unsused stuff
- Updated Elm-UI version from 1.1.5 to 1.1.7
- OxHead.elm file renamed to Main.elm

## [0.28.0] - 2020-05-29

### Added

- Early Visualization Implementation
- Epoch Progression

### Changed

- Pending Slot Leader Are Now Sorted
- Monospaced Font Again

## [0.27.0] - 2020-05-20

### Added

- Color-coded tables by leader logs status
- Scrollbar Style

### Changed

- View title style
- Normal Font
- Monospaced Font

### Removed

- Iosevka Self-Host Font

## [0.26.0] - 2020-02-18

### Added

- Somewhat Mobile Friendly

### Changed

- Refactor on Navigation Bar

### Removed

- onFocus Style

## [0.25.1] - 2020-02-10

### Added

- Element.Lazy for better performance
- Format to some errors

### Changed

- Loading Spinner Color


## [0.25.0] - 2020-02-07

### Added

- [RemoteData Package](https://package.elm-lang.org/packages/krisajenkins/remotedata/latest/)
- Message when Data is yet to be Fetched
- Spinner when Loading Data

### Changed

- Fetched Data is now represented with RemoteData Datatype

## [0.24.0] - 2020-01-28

### Added

- Support for differents Node Status

## [0.23.0] - 2020-01-27

### Added

- Link to Selected Stake Pool on Cardano Explorer

## [0.22.1] - 2020-01-26

### Changed

- Ratio Numerator/Denominator to Percentage

## [0.22.0] - 2020-01-20

### Added

- Stake Pool Size
- Stake Pool Saturation

## [0.21.0] - 2020-01-19

### Added

- Set your Stake Pool ID on Settings
- Highlight Stake Pool ID on Distribution view table

### Changed

- Load Stake Pool Details in Distribution view if a Stake Pool ID is set on Settings
- Clear Stake Pool Details if theres no match in Distribution view

## [0.20.2] - 2020-01-04

### Added

- Missing ADA formatting on Dangling value

### Changed

- Uptime from seconds to relative time

## [0.20.1] - 2020-01-03

### Added

- CSS to import Fonts
- Iosevka Font
- Store window size on model

### Changed

- Source Code Pro to Iosevka
- Sidebar Size


## [0.20.0] - 2019-12-31

### Added

- Stake Pool ID Suggestions

## [0.19.0] - 2019-12-29

### Added

- Welcome View

### Changed

- Fixed Ox Head logo

## [0.18.0] - 2019-12-23

### Added

- Foundations for Elm-UI responsive behavior

## [0.17.2] - 2019-12-19

### Changed

- Posix to Human Readable Date-Time now respects Timezone Offset

## [0.17.1] - 2019-12-18

### Added

- Tooltips on Stake Pool Details

## [0.17.0] - 2019-12-17

### Added

- Click a Stake Pool ID for Details
- Stake Pool ID Input Field
- Stake Pool Details
- Missing Format in ADA Values

## Changed

- Node View Layout
- Stake Distribution Layout

## [0.16.0] - 2019-12-16

### Added

- [DateFormat Package](https://package.elm-lang.org/packages/ryannhg/date-format/latest/)
- Current Time to compare with schedules
- Countdown to next block
- Stake Pools Leaders sorted by `created_at`
- [Elm ISO8601 Date Strings Package](https://package.elm-lang.org/packages/rtfeldman/elm-iso8601-date-strings/latest/)
  
### Changed

- Date Decoders from String to Time.Posix
- Date Format Utils

## [0.15.2] - 2019-12-13

### Changed

- Request are made according to the current Route

## [0.15.1] - 2019-12-12

### Changed

- Route path changed from stakepools to distribution

## [0.15.0] - 2019-12-09

### Added

- Show New Stats Data
- Show New Settings Data

### Changed

- Stats API Decoder
- Settings API Decoder

## [0.14.0] - 2019-12-03

### Added

- Local Storage support for Node URL

## [0.13.5] - 2019-12-03

### Added

- Type signature for SVG elements

### Changed

- SVG Elements are now wrapped in HTML Element

## [0.13.4] - 2019-11-30

### Added

- Favicon
- Repo Changelog

## [0.13.3] - 2019-11-28

### Changed

- ADA Value Formating
- Sidebar UI
- Page Name Stake Pool Leaders to Stake Leaders Logs

## [0.13.2] - 2019-11-26

### Added

- Highlight Sidebar items on Hover/Active Page

### Removed

- SVG Assets

## [0.13.1] - 2019-11-25

### Added

- SVGs in Elm

### Changed

- Copy index.html as 404.html in Gitlab CI

## [0.13.0] - 2019-11-24

### Added

- Community Request:
  - Stake Pool Leaders Page


## [0.12.0] - 2019-11-22

### Added

- Community Request:
  - Stake Pool Distribution Page
    - Sort by Stake
    - Show/Hide Stake Pools with 0 ADA

### Changed

- Stake Pools UI

## [0.11.2] - 2019-11-18

### Added

- Help in Settings Page
- Support Project in About Page

### Changed

- Gitlab CI to move Assets folder to public
- Assets

## [0.11.1] - 2019-11-17

### Added

- Datetime Format
- Gitlab CI

### Changed

- UI

## [0.11.0] - 2019-11-16

### Added

- Tooltips

### Changed

- Stake Pools Page


## [0.10.0] - 2019-11-14

### Added

- Navigation
- Assets
- Settings Page
- About Page
- Stake Pools Page
- Count Stake Pools

### Changed

- UI

## [0.9.0] - 2019-11-09

### Added

- Application Logo
- Sidebar
- Collapsable Data Panels
- Status Icon
- Count Nodes on Network Status

### Changed

- Fonts
- Color Palettes

## [0.8.1] - 2019-11-07

### Added

- Fonts
- Color Palettes

## [0.8.0] - 2019-11-06

### Added

- Support for the following API paths
  - Node Stats

## [0.7.0] - 2019-10-31

### Added

- Support for the following API paths
  - Leader
  - Block
  - Account
  - Shutdown

## [0.6.0] - 2019-10-27

### Added

- Support for the following API paths
  - Leaders
  - Leaders Logs
  - Fragment Logs

## [0.5.0] - 2019-10-26

### Added

- Support for the following API paths
  - Stake

## [0.4.0] - 2019-10-25

### Added

- Support for the following API paths
  - Settings
  - UTXO
  - Tip
  - Stake Pools

## [0.3.0] - 2019-10-24

### Added

- Support for the following API paths
  - Network Status

## [0.2.1] - 2019-10-23

### Added

- [Elm UI Library](https://package.elm-lang.org/packages/mdgriffith/elm-ui/latest/)

### Changed

- Elm Version to 0.19.1

## [0.1.0] - 2019-10-18

### Added

- README.md
- Edit Node URL and toggle Monitoring

## [0.0.1] - 2019-10-17

### Added
  
- First succesful request to the Node
