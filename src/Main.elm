port module Main exposing (Model, Msg, init, subscriptions, update, view)

import Browser
import Browser.Events
import Browser.Navigation as Nav
import DateFormat.Relative as RelativeDateFormat
import Element exposing (Attr, Device, DeviceClass(..), Element, IndexedColumn, Orientation(..), alignBottom, alignLeft, alignRight, alignTop, centerX, centerY, classifyDevice, clip, column, el, fill, fillPortion, height, image, inFront, link, maximum, minimum, moveRight, newTabLink, onLeft, onRight, padding, paddingEach, paddingXY, pointer, px, rgb255, row, scrollbarY, spaceEvenly, spacing, text, width, wrappedRow)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Lazy as Lazy
import Element.Region as Region
import FormatNumber
import Html.Attributes
import Http exposing (get)
import Iso8601
import Json.Decode exposing (index, int, list, map2, string)
import Json.Decode.Pipeline exposing (required)
import RemoteData exposing (RemoteData(..), WebData)
import Svg exposing (circle, defs, g, polygon, polyline, rect, svg)
import Svg.Attributes as SvgA
import Task
import Time exposing (Posix)
import Url
import Url.Parser exposing (Parser, int, map, oneOf, parse, s, top)
import Visualization exposing (viewEpoch)



-- Init


init : Flags -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        initModel =
            initialModel url key
    in
    ( { initModel | nodeUrl = flags.userSettings.nodeURL, myStakePoolID = flags.userSettings.stakePoolID, stakePoolID = flags.userSettings.stakePoolID, device = device flags.width flags.height, width = flags.width, height = flags.height }, Task.perform AdjustTimeZone Time.here )


type alias Flags =
    { width : Int
    , height : Int
    , userSettings : UserSettings
    }


device : Int -> Int -> Device
device width height =
    classifyDevice
        { width = width
        , height = height
        }


type alias UserSettings =
    { nodeURL : String
    , stakePoolID : String
    }



-- Model


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , showMenu : Bool
    , status : Status
    , nodeUrl : String
    , isMonitoring : Bool
    , nodeStats : WebData NodeStats
    , showNodeStats : Bool
    , networkStatsList : WebData (List NetworkStats)
    , showNetworkStats : Bool
    , nodeSettings : WebData NodeSettings
    , showNodeSettings : Bool

    -- , utxoList : WebData (List UTXO)
    -- , showUTXOList : Bool
    -- , tip : WebData String
    -- , showTip : Bool
    , stakePoolsList : WebData (List String)
    , showStakePoolsList : Bool
    , stakeDistribution : WebData StakeDistribution
    , showStakeDistribution : Bool
    , stakePoolDetail : WebData StakePool
    , stakePoolID : String
    , myStakePoolID : String
    , showZeroStaked : Bool
    , sortAscending : Bool
    , leaders : WebData (List Int)
    , showLeaders : Bool
    , leadersLogs : WebData (List LeadersLogs)
    , showLeadersLogsList : Bool

    -- , fragmentLogs : WebData (List FragmentLogs)
    -- , showFragmentLogsList : Bool
    -- , block : WebData String
    -- , nextBlock : WebData String
    -- , account : WebData Account
    -- , blockId : String
    -- , accountId : String
    -- , nextBlockCount : Int
    -- , leaderId : String
    , error : String
    , showStakePoolInputSuggestions : Bool
    , zone : Time.Zone
    , time : Posix
    , device : Element.Device
    , width : Int
    , height : Int
    }


initialModel : Url.Url -> Nav.Key -> Model
initialModel url key =
    { url = url
    , key = key
    , showMenu = False
    , status = Loaded
    , nodeUrl = "http://127.0.0.1:3100"
    , isMonitoring = False
    , nodeStats = NotAsked
    , showNodeStats = True
    , showNetworkStats = False
    , networkStatsList = NotAsked
    , nodeSettings = NotAsked
    , showNodeSettings = True

    -- , utxoList = NotAsked
    -- , tip = NotAsked
    -- , showUTXOList = True
    , stakePoolsList = NotAsked
    , stakeDistribution = NotAsked
    , stakePoolDetail = NotAsked
    , stakePoolID = ""
    , myStakePoolID = ""

    -- , showTip = True
    , leaders = NotAsked
    , leadersLogs = NotAsked
    , showStakePoolsList = True

    -- , fragmentLogs = NotAsked
    -- , block = NotAsked
    , showStakeDistribution = True
    , showZeroStaked = False
    , sortAscending = False

    -- , nextBlock = NotAsked
    -- , account = NotAsked
    , showLeaders = True

    -- , blockId = ""
    -- , accountId = ""
    , showLeadersLogsList = True

    -- , nextBlockCount = 1
    -- , leaderId = ""
    -- , showFragmentLogsList = True
    , error = ""
    , showStakePoolInputSuggestions = False
    , zone = Time.utc
    , time = Time.millisToPosix 0
    , device = Device Desktop Landscape
    , width = 1920
    , height = 1080
    }


port setStorage : UserSettings -> Cmd msg



-- Navigation


type Route
    = Home
    | Node
    | Distribution
    | Leaders
    | Settings
    | About
    | NotFound


route : Parser (Route -> a) a
route =
    oneOf
        [ map Home top
        , map Node (s "node")
        , map Distribution (s "distribution")
        , map Leaders (s "leaders")
        , map Settings (s "settings")
        , map About (s "about")
        ]


toRoute : String -> Route
toRoute string =
    case Url.fromString string of
        Nothing ->
            NotFound

        Just url ->
            Maybe.withDefault NotFound (parse route url)



-- Msg


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | FetchDataClicked
    | GotNodeStatsMessage (WebData NodeStats)
    | GotNetworkStatsListMessage (WebData (List NetworkStats))
    | GotNodeSettingsMessage (WebData NodeSettings)
      -- | GotUTXOListMessage (WebData (List UTXO))
      -- | GotTipMessage (WebData String)
      -- | GotStakePoolsListMessage (WebData (List String))
    | GotStakePoolMessage (WebData StakePool)
    | GotStakeDistributionMessage (WebData StakeDistribution)
      -- | GotLeadersMessage (WebData (List Int))
    | GotLeadersLogsListMessage (WebData (List LeadersLogs))
      -- | GotFragmentLogsListMessage (WebData (List FragmentLogs))
      -- | GotBlockMessage (WebData String)
      -- | GotNextBlockMessage (WebData String)
      -- | GotAccountMessage (WebData Account)
      -- | GotShutdownMessage (WebData String)
      -- | GotDeleteLeaderMessage (WebData String)
    | ToggleMonitoring
    | ToggleNodeStatsPanel
    | ToggleNodeSettingsPanel
    | ToggleNetworkStatsPanel
      -- | ToggleUTXOPanel
      -- | ToggleTipPanel
      -- | ToggleStakePoolsPanel
      -- | ToggleStakeDistributionPanel
      -- | ToggleLeadersPanel
      -- | ToggleLeadersLogsListPanel
      -- -- | ToggleFragmentLogsPanel
    | ToggleShowZeroStaked
    | ToggleSortAscending
    | ToggleMenu
    | Monitor Posix
    | NodeURLChanged String
    | MyStakePoolIDChanged String
      -- | BlockIdChanged String
      -- | NextBlockChanged String
      -- | AccountIdChanged String
      -- | LeaderIdChanged String
    | StakePoolIdClicked String
    | StakePoolIdChanged String
      -- | GetBlockClicked
      -- | GetNextBlockClicked
      -- | GetAccountClicked
      -- | DeleteLeaderClicked
      -- | GetShutdownClicked
    | AdjustTimeZone Time.Zone
    | Tick Posix
    | WindowResized Int Int
    | ActivateStakePoolInput
    | DeactivateStakePoolInput



-- Status


type Status
    = Fetching
    | Loaded
    | Errored String



-- NetworkStats


type alias NetworkStats =
    { addr : String
    , establishedAt : Posix
    , lastBlockReceived : Maybe Posix
    , lastFragmentReceived : Maybe Posix
    , lastGossipReceived : Maybe Posix
    }


decodeNetworkStats : Json.Decode.Decoder NetworkStats
decodeNetworkStats =
    Json.Decode.succeed NetworkStats
        |> Json.Decode.Pipeline.required "addr" Json.Decode.string
        |> Json.Decode.Pipeline.required "establishedAt" Iso8601.decoder
        |> Json.Decode.Pipeline.optional "lastBlockReceived" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.optional "lastFragmentReceived" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.optional "lastGossipReceived" (Json.Decode.maybe Iso8601.decoder) Nothing


getNetworkStatsList : Model -> Cmd Msg
getNetworkStatsList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/network/stats"
        , expect = expectJson (RemoteData.fromResult >> GotNetworkStatsListMessage) (list decodeNetworkStats)
        }



-- NodeStats


type NodeStats
    = Initializing BootstrappingNodeStats
    | Running RunningNodeStats


type alias BootstrappingNodeStats =
    { version : String
    , state : String
    }


type alias RunningNodeStats =
    { version : String
    , state : String
    , blockRecvCnt : Int
    , lastBlockContentSize : Int
    , lastBlockDate : String
    , lastBlockFees : Int
    , lastBlockHash : String
    , lastBlockHeight : String
    , lastBlockSum : Int
    , lastBlockTime : Maybe Posix
    , lastBlockTx : Int
    , txRecvCnt : Int
    , uptime : Posix
    }


decodeBootstrappingNodeStats : Json.Decode.Decoder BootstrappingNodeStats
decodeBootstrappingNodeStats =
    Json.Decode.succeed BootstrappingNodeStats
        |> Json.Decode.Pipeline.required "version" Json.Decode.string
        |> Json.Decode.Pipeline.required "state" Json.Decode.string


decodeRunningNodeStats : Json.Decode.Decoder RunningNodeStats
decodeRunningNodeStats =
    Json.Decode.succeed RunningNodeStats
        |> Json.Decode.Pipeline.required "version" Json.Decode.string
        |> Json.Decode.Pipeline.required "state" Json.Decode.string
        |> Json.Decode.Pipeline.required "blockRecvCnt" Json.Decode.int
        |> Json.Decode.Pipeline.required "lastBlockContentSize" Json.Decode.int
        |> Json.Decode.Pipeline.required "lastBlockDate" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockFees" Json.Decode.int
        |> Json.Decode.Pipeline.required "lastBlockHash" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockHeight" Json.Decode.string
        |> Json.Decode.Pipeline.required "lastBlockSum" Json.Decode.int
        |> Json.Decode.Pipeline.optional "lastBlockTime" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.required "lastBlockTx" Json.Decode.int
        |> Json.Decode.Pipeline.required "txRecvCnt" Json.Decode.int
        |> Json.Decode.Pipeline.required "uptime" (Json.Decode.map (\uptime -> Time.millisToPosix (uptime * 1000)) Json.Decode.int)


getNodeStats : Model -> Cmd Msg
getNodeStats model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/node/stats"
        , expect = expectJson (RemoteData.fromResult >> GotNodeStatsMessage) (Json.Decode.oneOf [ Json.Decode.map Running decodeRunningNodeStats, Json.Decode.map Initializing decodeBootstrappingNodeStats ])
        }



-- Settings


type alias NodeSettings =
    { block0Hash : String
    , block0Time : Posix
    , consensusVersion : String
    , currSlotStartTime : Maybe Posix
    , blockContentMaxSize : Int
    , epochStabilityDepth : Int
    , fees : NodeSettingsFees
    , rewardParams : NodeSettingsRewardParams
    , slotDuration : Int
    , slotsPerEpoch : Int
    , treasuryTax : NodeSettingsTreasuryTax
    }


type alias NodeSettingsFees =
    { certificate : Int
    , coefficient : Int
    , constant : Int
    }


type alias NodeSettingsRewardParams =
    { compoundingRatio : Ratio
    , compoundingType : String
    , epochRate : Int
    , epochStart : Int
    , initialValue : Int
    }


type alias NodeSettingsTreasuryTax =
    { fixed : Int
    , ratio : Ratio
    }


decodeNodeSettings : Json.Decode.Decoder NodeSettings
decodeNodeSettings =
    Json.Decode.succeed NodeSettings
        |> Json.Decode.Pipeline.required "block0Hash" Json.Decode.string
        |> Json.Decode.Pipeline.required "block0Time" Iso8601.decoder
        |> Json.Decode.Pipeline.required "consensusVersion" Json.Decode.string
        |> Json.Decode.Pipeline.optional "currSlotStartTime" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.required "blockContentMaxSize" Json.Decode.int
        |> Json.Decode.Pipeline.required "epochStabilityDepth" Json.Decode.int
        |> Json.Decode.Pipeline.required "fees" decodeNodeSettingsFees
        |> Json.Decode.Pipeline.required "rewardParams" decodeNodeSettingsRewardParams
        |> Json.Decode.Pipeline.required "slotDuration" Json.Decode.int
        |> Json.Decode.Pipeline.required "slotsPerEpoch" Json.Decode.int
        |> Json.Decode.Pipeline.required "treasuryTax" decodeNodeSettingsTreasuryTax


decodeNodeSettingsFees : Json.Decode.Decoder NodeSettingsFees
decodeNodeSettingsFees =
    Json.Decode.succeed NodeSettingsFees
        |> Json.Decode.Pipeline.required "certificate" Json.Decode.int
        |> Json.Decode.Pipeline.required "coefficient" Json.Decode.int
        |> Json.Decode.Pipeline.required "constant" Json.Decode.int


decodeNodeSettingsRewardParams : Json.Decode.Decoder NodeSettingsRewardParams
decodeNodeSettingsRewardParams =
    Json.Decode.succeed NodeSettingsRewardParams
        |> Json.Decode.Pipeline.required "compoundingRatio" decodeRatio
        |> Json.Decode.Pipeline.required "compoundingType" Json.Decode.string
        |> Json.Decode.Pipeline.required "epochRate" Json.Decode.int
        |> Json.Decode.Pipeline.required "epochStart" Json.Decode.int
        |> Json.Decode.Pipeline.required "initialValue" Json.Decode.int


decodeNodeSettingsTreasuryTax : Json.Decode.Decoder NodeSettingsTreasuryTax
decodeNodeSettingsTreasuryTax =
    Json.Decode.succeed NodeSettingsTreasuryTax
        |> Json.Decode.Pipeline.required "fixed" Json.Decode.int
        |> Json.Decode.Pipeline.required "ratio" decodeRatio


decodeRatio : Json.Decode.Decoder Ratio
decodeRatio =
    Json.Decode.succeed Ratio
        |> Json.Decode.Pipeline.required "denominator" Json.Decode.int
        |> Json.Decode.Pipeline.required "numerator" Json.Decode.int


getNodeSettings : Model -> Cmd Msg
getNodeSettings model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/settings"
        , expect = expectJson (RemoteData.fromResult >> GotNodeSettingsMessage) decodeNodeSettings
        }



-- UTXO
-- type alias UTXO =
--     { transaction_id : String
--     , index_in_transaction : Int
--     , address : String
--     , associated_fund : Int
--     }
-- decodeUTXO : Json.Decode.Decoder UTXO
-- decodeUTXO =
--     Json.Decode.succeed UTXO
--         |> Json.Decode.Pipeline.required "transaction_id" Json.Decode.string
--         |> Json.Decode.Pipeline.required "index_in_transaction" Json.Decode.int
--         |> Json.Decode.Pipeline.required "address" Json.Decode.string
--         |> Json.Decode.Pipeline.required "associated_fund" Json.Decode.int
-- getUTXOList : Model -> Cmd Msg
-- getUTXOList model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/utxo"
--         , expect = expectJson (RemoteData.fromResult >> GotUTXOListMessage) (list decodeUTXO)
--         }
-- Tip
-- getTip : Model -> Cmd Msg
-- getTip model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/tip"
--         , expect = Http.expectString (RemoteData.fromResult >> GotTipMessage)
--         }
-- Stake Pools


type alias StakePool =
    { tax : StakePoolTax
    , rewards : StakePoolRewards
    , total_stake : Int
    , kesPublicKey : String
    , vrfPublicKey : String
    }



-- Ratio is a common type across various responses


type alias Ratio =
    { numerator : Int
    , denominator : Int
    }


type alias StakePoolTax =
    { fixed : Int
    , ratio : Ratio
    , max : Int
    }


type alias StakePoolRewards =
    { epoch : Int
    , value_taxed : Int
    , value_for_stakers : Int
    }


decodeStakePool : Json.Decode.Decoder StakePool
decodeStakePool =
    Json.Decode.succeed StakePool
        |> Json.Decode.Pipeline.required "tax" decodeStakePoolTax
        |> Json.Decode.Pipeline.required "rewards" decodeStakePoolRewards
        |> Json.Decode.Pipeline.required "total_stake" Json.Decode.int
        |> Json.Decode.Pipeline.required "kes_public_key" Json.Decode.string
        |> Json.Decode.Pipeline.required "vrf_public_key" Json.Decode.string


decodeStakePoolTax : Json.Decode.Decoder StakePoolTax
decodeStakePoolTax =
    Json.Decode.succeed StakePoolTax
        |> Json.Decode.Pipeline.required "fixed" Json.Decode.int
        |> Json.Decode.Pipeline.required "ratio" decodeRatio
        |> Json.Decode.Pipeline.optional "max" Json.Decode.int 0


decodeStakePoolRewards : Json.Decode.Decoder StakePoolRewards
decodeStakePoolRewards =
    Json.Decode.succeed StakePoolRewards
        |> Json.Decode.Pipeline.required "epoch" Json.Decode.int
        |> Json.Decode.Pipeline.required "value_taxed" Json.Decode.int
        |> Json.Decode.Pipeline.required "value_for_stakers" Json.Decode.int


getStakePool : Model -> String -> Cmd Msg
getStakePool model stakePoolID =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/stake_pool/" ++ stakePoolID
        , expect = Http.expectJson (RemoteData.fromResult >> GotStakePoolMessage) decodeStakePool
        }



-- Stake Distribution


type alias StakeDistribution =
    { epoch : Int
    , stake : StakeDetail
    }


type alias StakeDetail =
    { dangling : Int
    , pools : List Pool
    , unassigned : Int
    }


type alias Pool =
    { id : String
    , value : Int
    }


decodeStakeDistribution : Json.Decode.Decoder StakeDistribution
decodeStakeDistribution =
    Json.Decode.succeed StakeDistribution
        |> Json.Decode.Pipeline.required "epoch" Json.Decode.int
        |> Json.Decode.Pipeline.required "stake" decodeStakeDetail


decodeStakeDetail : Json.Decode.Decoder StakeDetail
decodeStakeDetail =
    Json.Decode.succeed StakeDetail
        |> Json.Decode.Pipeline.required "dangling" Json.Decode.int
        |> Json.Decode.Pipeline.required "pools" (Json.Decode.list (Json.Decode.map2 Pool (index 0 Json.Decode.string) (index 1 Json.Decode.int)))
        |> Json.Decode.Pipeline.required "unassigned" Json.Decode.int


getStakeDistribution : Model -> Cmd Msg
getStakeDistribution model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/stake"
        , expect = expectJson (RemoteData.fromResult >> GotStakeDistributionMessage) decodeStakeDistribution
        }



-- Leaders
-- getLeaders : Model -> Cmd Msg
-- getLeaders model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/leaders"
--         , expect = Http.expectJson (RemoteData.fromResult >> GotLeadersMessage) (Json.Decode.list Json.Decode.int)
--         }
-- Leader Logs


type alias LeadersLogs =
    { created_at_time : Posix
    , scheduled_at_time : Posix
    , scheduled_at_date : String
    , wake_at_time : Maybe Posix
    , finished_at_time : Maybe Posix
    , status : LeaderLogsStatus
    , enclave_leader_id : Int
    }


type LeaderLogsStatus
    = Pending String
    | Rejected LeaderLogsStatusRejected
    | Created LeaderLogsStatusCreated


type alias LeaderLogsCreatedBlock =
    { block : String
    , chain_length : Int
    }


type alias LeaderLogsStatusCreated =
    { block : LeaderLogsCreatedBlock
    }


type alias LeaderLogsRejectedBlock =
    { reason : String
    }


type alias LeaderLogsStatusRejected =
    { rejected : LeaderLogsRejectedBlock
    }


decodeLeadersLogs : Json.Decode.Decoder LeadersLogs
decodeLeadersLogs =
    Json.Decode.succeed LeadersLogs
        |> Json.Decode.Pipeline.required "created_at_time" Iso8601.decoder
        |> Json.Decode.Pipeline.required "scheduled_at_time" Iso8601.decoder
        |> Json.Decode.Pipeline.required "scheduled_at_date" Json.Decode.string
        |> Json.Decode.Pipeline.optional "wake_at_time" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.optional "finished_at_time" (Json.Decode.maybe Iso8601.decoder) Nothing
        |> Json.Decode.Pipeline.optional "status" (Json.Decode.oneOf [ Json.Decode.map Pending Json.Decode.string, Json.Decode.map Created decodeLeaderLogsStatusCreated, Json.Decode.map Rejected decodeLeaderLogsStatusRejected ]) (Pending "")
        |> Json.Decode.Pipeline.required "enclave_leader_id" Json.Decode.int


decodeLeaderLogsCreatedBlock : Json.Decode.Decoder LeaderLogsCreatedBlock
decodeLeaderLogsCreatedBlock =
    Json.Decode.succeed LeaderLogsCreatedBlock
        |> Json.Decode.Pipeline.required "block" Json.Decode.string
        |> Json.Decode.Pipeline.required "chain_length" Json.Decode.int


decodeLeaderLogsRejectedBlock : Json.Decode.Decoder LeaderLogsRejectedBlock
decodeLeaderLogsRejectedBlock =
    Json.Decode.succeed LeaderLogsRejectedBlock
        |> Json.Decode.Pipeline.required "reason" Json.Decode.string


decodeLeaderLogsStatusRejected : Json.Decode.Decoder LeaderLogsStatusRejected
decodeLeaderLogsStatusRejected =
    Json.Decode.succeed LeaderLogsStatusRejected
        |> Json.Decode.Pipeline.required "Rejected" decodeLeaderLogsRejectedBlock


decodeLeaderLogsStatusCreated : Json.Decode.Decoder LeaderLogsStatusCreated
decodeLeaderLogsStatusCreated =
    Json.Decode.succeed LeaderLogsStatusCreated
        |> Json.Decode.Pipeline.required "Block" decodeLeaderLogsCreatedBlock


getLeadersLogsList : Model -> Cmd Msg
getLeadersLogsList model =
    Http.get
        { url = model.nodeUrl ++ "/api/v0/leaders/logs"
        , expect = expectJson (RemoteData.fromResult >> GotLeadersLogsListMessage) (list decodeLeadersLogs)
        }



-- Fragment Logs
-- type alias FragmentLogs =
--     { fragment_id : String
--     , received_from : String
--     , received_at : Posix
--     , last_updated_at : Posix
--     , status : String
--     }
-- decodeFragmentLogs : Json.Decode.Decoder FragmentLogs
-- decodeFragmentLogs =
--     Json.Decode.succeed FragmentLogs
--         |> Json.Decode.Pipeline.required "fragment_id" Json.Decode.string
--         |> Json.Decode.Pipeline.required "received_from" Json.Decode.string
--         |> Json.Decode.Pipeline.required "received_at" Iso8601.decoder
--         |> Json.Decode.Pipeline.required "last_updated_at" Iso8601.decoder
--         |> Json.Decode.Pipeline.required "status" Json.Decode.string
-- getFragmentLogsList : Model -> Cmd Msg
-- getFragmentLogsList model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/fragment/logs"
--         , expect = expectJson (RemoteData.fromResult >> GotFragmentLogsListMessage) (list decodeFragmentLogs)
--         }
-- Block
-- getBlock : Model -> Cmd Msg
-- getBlock model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/block/" ++ model.blockId
--         , expect = Http.expectString (RemoteData.fromResult >> GotBlockMessage)
--         }
-- Next Block
-- getNextBlock : Model -> Cmd Msg
-- getNextBlock model =
--     Http.get
--         { url =
--             model.nodeUrl
--                 ++ "/api/v0/block/"
--                 ++ model.blockId
--                 ++ "/next_id"
--                 ++ (if model.nextBlockCount > 1 then
--                         "?nextBlockCount=" ++ String.fromInt model.nextBlockCount
--                     else
--                         ""
--                    )
--         , expect = Http.expectString (RemoteData.fromResult >> GotNextBlockMessage)
--         }
-- Account
-- type alias Account =
--     { delegation : AccountDelegation
--     , value : Int
--     , counter : Int
--     }
-- type alias AccountDelegation =
--     { pools : List String
--     }
-- decodeAccount : Json.Decode.Decoder Account
-- decodeAccount =
--     Json.Decode.succeed Account
--         |> Json.Decode.Pipeline.required "delegation" decodeAccountDelegation
--         |> Json.Decode.Pipeline.required "value" Json.Decode.int
--         |> Json.Decode.Pipeline.required "counter" Json.Decode.int
-- decodeAccountDelegation : Json.Decode.Decoder AccountDelegation
-- decodeAccountDelegation =
--     Json.Decode.succeed AccountDelegation
--         |> Json.Decode.Pipeline.required "pools" (Json.Decode.list Json.Decode.string)
-- getAccount : Model -> Cmd Msg
-- getAccount model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/account/" ++ model.accountId
--         , expect = Http.expectJson (RemoteData.fromResult >> GotAccountMessage) decodeAccount
--         }
-- Delete Leader
-- deleteLeader : Model -> Cmd Msg
-- deleteLeader model =
--     Http.request
--         { method = "DELETE"
--         , headers = []
--         , url = model.nodeUrl ++ "/api/v0/leaders/" ++ model.leaderId
--         , body = Http.emptyBody
--         , expect = Http.expectString (RemoteData.fromResult >> GotDeleteLeaderMessage)
--         , timeout = Nothing
--         , tracker = Nothing
--         }
-- Shutdown
-- getShutdown : Model -> Cmd Msg
-- getShutdown model =
--     Http.get
--         { url = model.nodeUrl ++ "/api/v0/shutdown"
--         , expect = Http.expectString (RemoteData.fromResult >> GotShutdownMessage)
--         }
-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url, showMenu = False }, Cmd.none )

        FetchDataClicked ->
            case toRoute (Url.toString model.url) of
                Node ->
                    ( { model | status = Fetching, nodeStats = Loading, networkStatsList = Loading, nodeSettings = Loading }, Cmd.batch <| List.map (\x -> x model) [ getNodeStats, getNetworkStatsList, getNodeSettings ] )

                Distribution ->
                    ( { model | status = Fetching, stakeDistribution = Loading }
                    , if model.stakePoolDetail == NotAsked && not (String.isEmpty model.stakePoolID) then
                        Cmd.batch [ getStakeDistribution model, getStakePool model model.stakePoolID ]

                      else
                        getStakeDistribution model
                    )

                Leaders ->
                    ( { model | status = Fetching, leadersLogs = Loading }, getLeadersLogsList model )

                _ ->
                    ( model, Cmd.none )

        -- GetBlockClicked ->
        --     ( model
        --     , getBlock model
        --     )
        -- GetNextBlockClicked ->
        --     ( model
        --     , getNextBlock model
        --     )
        -- GetAccountClicked ->
        --     ( model
        --     , getAccount model
        --     )
        -- DeleteLeaderClicked ->
        --     ( model
        --     , deleteLeader model
        --     )
        -- GetShutdownClicked ->
        --     ( model
        --     , getShutdown model
        --     )
        ToggleMonitoring ->
            ( { model | isMonitoring = not model.isMonitoring }, Cmd.none )

        Monitor _ ->
            case toRoute (Url.toString model.url) of
                Node ->
                    ( { model | status = Fetching }, Cmd.batch <| List.map (\x -> x model) [ getNodeStats, getNetworkStatsList, getNodeSettings ] )

                Distribution ->
                    ( { model | status = Fetching }
                    , if model.stakePoolDetail == NotAsked && not (String.isEmpty model.stakePoolID) then
                        Cmd.batch [ getStakeDistribution model, getStakePool model model.stakePoolID ]

                      else
                        getStakeDistribution model
                    )

                Leaders ->
                    ( { model | status = Fetching }, getLeadersLogsList model )

                _ ->
                    ( model, Cmd.none )

        ToggleNodeStatsPanel ->
            ( { model | showNodeStats = not model.showNodeStats }, Cmd.none )

        ToggleNodeSettingsPanel ->
            ( { model | showNodeSettings = not model.showNodeSettings }, Cmd.none )

        ToggleNetworkStatsPanel ->
            ( { model | showNetworkStats = not model.showNetworkStats }, Cmd.none )

        -- ToggleUTXOPanel ->
        --     ( { model | showUTXOList = not model.showUTXOList }, Cmd.none )
        -- ToggleTipPanel ->
        --     ( { model | showTip = not model.showTip }, Cmd.none )
        -- ToggleStakePoolsPanel ->
        --     ( { model | showStakePoolsList = not model.showStakePoolsList }, Cmd.none )
        -- ToggleStakeDistributionPanel ->
        --     ( { model | showStakeDistribution = not model.showStakeDistribution }, Cmd.none )
        ToggleShowZeroStaked ->
            ( { model | showZeroStaked = not model.showZeroStaked }, Cmd.none )

        ToggleSortAscending ->
            ( { model | sortAscending = not model.sortAscending }, Cmd.none )

        -- ToggleLeadersPanel ->
        --     ( { model | showLeaders = not model.showLeaders }, Cmd.none )
        -- ToggleLeadersLogsListPanel ->
        --     ( { model | showLeadersLogsList = not model.showLeadersLogsList }, Cmd.none )
        -- ToggleFragmentLogsPanel ->
        --     ( { model | showFragmentLogsList = not model.showFragmentLogsList }, Cmd.none )
        ToggleMenu ->
            ( { model | showMenu = not model.showMenu }, Cmd.none )

        GotNodeStatsMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , nodeStats = response
              }
            , Cmd.none
            )

        GotNetworkStatsListMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , networkStatsList = response
              }
            , Cmd.none
            )

        GotNodeSettingsMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , nodeSettings = response
              }
            , Cmd.none
            )

        -- GotUTXOListMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , utxoList = response
        --       }
        --     , Cmd.none
        --     )
        -- GotTipMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , tip = response
        --       }
        --     , Cmd.none
        --     )
        -- GotStakePoolsListMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , stakePoolsList = response
        --       }
        --     , Cmd.none
        --     )
        GotStakeDistributionMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , stakeDistribution = response
              }
            , Cmd.none
            )

        GotStakePoolMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , stakePoolDetail = response
              }
            , Cmd.none
            )

        -- GotLeadersMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , leaders = response
        --       }
        --     , Cmd.none
        --     )
        GotLeadersLogsListMessage response ->
            ( { model
                | status =
                    if RemoteData.isFailure model.nodeStats then
                        Errored ""

                    else
                        Loaded
                , leadersLogs = response
              }
            , Cmd.none
            )

        -- GotFragmentLogsListMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , fragmentLogs = response
        --       }
        --     , Cmd.none
        --     )
        -- GotBlockMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , block = response
        --       }
        --     , Cmd.none
        --     )
        -- GotNextBlockMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , nextBlock = response
        --       }
        --     , Cmd.none
        --     )
        -- GotAccountMessage response ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --         , account = response
        --       }
        --     , Cmd.none
        --     )
        -- GotDeleteLeaderMessage _ ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --       }
        --     , Cmd.none
        --     )
        -- GotShutdownMessage _ ->
        --     ( { model
        --         | status =
        --             if RemoteData.isFailure model.nodeStats then
        --                 Errored ""
        --             else
        --                 Loaded
        --       }
        --     , Cmd.none
        --     )
        StakePoolIdClicked id ->
            ( { model | stakePoolID = id }, getStakePool model id )

        StakePoolIdChanged id ->
            if String.length id == 64 then
                ( { model | stakePoolID = id }, getStakePool model id )

            else
                ( { model | stakePoolID = id, stakePoolDetail = NotAsked }, Cmd.none )

        NodeURLChanged url ->
            let
                settings =
                    UserSettings url model.myStakePoolID
            in
            ( { model | nodeUrl = url }, setStorage settings )

        MyStakePoolIDChanged stakePoolID ->
            let
                settings =
                    UserSettings model.nodeUrl stakePoolID
            in
            ( { model | myStakePoolID = stakePoolID, stakePoolID = stakePoolID }, setStorage settings )

        -- BlockIdChanged id ->
        --     ( { model | blockId = id }, Cmd.none )
        -- AccountIdChanged id ->
        --     ( { model | accountId = id }, Cmd.none )
        -- LeaderIdChanged id ->
        --     ( { model | leaderId = id }, Cmd.none )
        -- NextBlockChanged count ->
        --     ( { model
        --         | nextBlockCount =
        --             case String.toInt count of
        --                 Just int ->
        --                     int
        --                 Nothing ->
        --                     1
        --       }
        --     , Cmd.none
        --     )
        AdjustTimeZone newZone ->
            ( { model | zone = newZone }
            , Cmd.none
            )

        Tick newTime ->
            ( { model | time = newTime }
            , Cmd.none
            )

        WindowResized width height ->
            ( { model | device = device width height, width = width, height = height, showMenu = False }, Cmd.none )

        ActivateStakePoolInput ->
            ( { model | showStakePoolInputSuggestions = True }, Cmd.none )

        DeactivateStakePoolInput ->
            ( { model | showStakePoolInputSuggestions = False }, Cmd.none )


expectJson : (Result Http.Error a -> msg) -> Json.Decode.Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (Http.BadUrl url)

                Http.Timeout_ ->
                    Err Http.Timeout

                Http.NetworkError_ ->
                    Err Http.NetworkError

                Http.BadStatus_ metadata _ ->
                    Err (Http.BadStatus metadata.statusCode)

                Http.GoodStatus_ _ body ->
                    case Json.Decode.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err (Http.BadBody (Json.Decode.errorToString err))



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.isMonitoring then
        Sub.batch [ Time.every 1000 Tick, Time.every 1000 Monitor, Browser.Events.onResize WindowResized ]

    else
        Sub.batch [ Time.every 1000 Tick, Browser.Events.onResize WindowResized ]



-- View


adaIcon : Element Msg
adaIcon =
    el [ width (px 12), centerY ] <| adaSvg


toggleIcon : Bool -> Element Msg
toggleIcon open =
    if open then
        el [] <| anchorSvg

    else
        el [ Element.rotate (degrees 180) ] <| anchorSvg


toggleShowHideIcon : Bool -> Element Msg
toggleShowHideIcon show =
    if show then
        el [] <| showSvg

    else
        el [] <| hideSvg


toggleSortingIcon : Bool -> Element Msg
toggleSortingIcon asc =
    if asc then
        el [] <| sortAscSvg

    else
        el [] <| sortDscSvg


view : Model -> Browser.Document Msg
view model =
    { title = "Ox Head"
    , body =
        [ Element.layoutWith
            { options =
                [ Element.focusStyle
                    { borderColor = Maybe.Nothing
                    , backgroundColor = Maybe.Nothing
                    , shadow = Maybe.Nothing
                    }
                ]
            }
            [ Background.color mainBackgroundColor, width (fill |> maximum model.width), height (fill |> maximum model.height), Font.color (Element.rgb255 45 45 45) ]
          <|
            Lazy.lazy viewMain model
        ]
    }


viewMain : Model -> Element Msg
viewMain model =
    let
        orientation =
            if model.device.class == Element.Phone || model.device.class == Element.Tablet then
                column
                    [ height fill
                    , width fill
                    , inFront <|
                        if model.showMenu then
                            column [ Element.moveDown 64, Background.color sidebarBackgroundColor, width (fill |> minimum model.width), height (fill |> maximum (model.height - 64)), spaceEvenly, paddingXY 0 64 ] <| navElements model

                        else
                            Element.none
                    ]

            else
                row [ height fill, width fill ]
    in
    orientation
        [ viewSidebar model
        , column [ width fill, height fill, spacing 10, scrollbarY ]
            [ -- , row []
              --     [ Input.text [ centerY, width (px 300) ] { onChange = BlockIdChanged, text = model.blockId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Block ID")), label = Input.labelLeft [ centerY ] (text "Block ID: ") }
              --     , Input.button buttonStyle { onPress = Just GetBlockClicked, label = text "Get Block" }
              --     , Input.text [ centerY, width (px 50) ] { onChange = NextBlockChanged, text = String.fromInt model.nextBlockCount, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Next Block Count")), label = Input.labelLeft [ centerY ] (text "Count: ") }
              --     , Input.button buttonStyle { onPress = Just GetNextBlockClicked, label = text "Get Next Block" }
              --     ]
              -- , row []
              --     [ Input.text [ centerY, width (px 300) ] { onChange = AccountIdChanged, text = model.accountId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Account ID")), label = Input.labelLeft [ centerY ] (text "Account ID: ") }
              --     , Input.button buttonStyle { onPress = Just GetAccountClicked, label = text "Get Account" }
              --     ]
              -- , row []
              --     [ Input.text [ centerY, width (px 300) ] { onChange = LeaderIdChanged, text = model.leaderId, placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Insert Leader ID")), label = Input.labelLeft [ centerY ] (text "Leader ID: ") }
              --     , Input.button buttonStyle { onPress = Just DeleteLeaderClicked, label = text "Delete Leader" }
              --     ]
              -- , row []
              --     [ Input.button buttonStyle { onPress = Just GetShutdownClicked, label = text "Shutdown" }
              --     ]
              -- , column []
              --     [ case model.block of
              --         Just block ->
              --             el [] <| text block
              --         Nothing ->
              --             Element.none
              --     , case model.nextBlock of
              --         Just block ->
              --             el [] <| text block
              --         Nothing ->
              --             Element.none
              --     ]
              case toRoute (Url.toString model.url) of
                Home ->
                    viewHome

                Node ->
                    if RemoteData.isSuccess model.nodeStats && RemoteData.isFailure model.nodeSettings && RemoteData.isFailure model.networkStatsList then
                        webDataView (viewNodeStats model.showNodeStats model.zone 0) model.nodeStats

                    else
                        webDataView (viewNode model) (merge3 model.nodeStats model.nodeSettings model.networkStatsList)

                Distribution ->
                    viewDistribution model

                Leaders ->
                    webDataView (viewLeadersLogs model.height model.time model.zone) model.leadersLogs

                Settings ->
                    viewSettings model

                About ->
                    viewAbout

                _ ->
                    Element.none
            ]
        ]


viewSidebar : Model -> Element Msg
viewSidebar model =
    if model.device.class == Element.Phone || model.device.class == Element.Tablet then
        row
            [ width (fill |> maximum model.width)
            , Background.color sidebarBackgroundColor
            , spacing 30
            , centerY
            , height (px 64)
            ]
            [ el
                [ Events.onClick ToggleMenu
                , padding 10
                , pointer
                , width (px 50)
                ]
              <|
                if model.showMenu then
                    openMenuSvg

                else
                    menuSvg
            , Input.button [ centerY ]
                { onPress = Just FetchDataClicked
                , label =
                    row [ centerY, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ], pointer ]
                        [ withTooltip "Fetch Data" <| fetchDataSvg ]
                }
            , Input.button
                [ centerY
                , Font.color
                    (if model.isMonitoring then
                        Element.rgb255 225 225 225

                     else
                        Element.rgb255 94 96 102
                    )
                , Element.mouseOver [ Font.color (Element.rgb255 225 225 225) ]
                , pointer
                ]
                { onPress = Just ToggleMonitoring
                , label =
                    row []
                        [ withTooltip "Monitor" <|
                            el
                                (if model.isMonitoring then
                                    [ Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ]

                                 else
                                    []
                                )
                            <|
                                monitorSvg
                        ]
                }
            , el
                [ centerY
                , alignRight
                , Background.color sidebarBackgroundColor
                , paddingXY 10 0
                ]
                (case model.status of
                    Fetching ->
                        el [ Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ] <| fetchingSvg

                    Loaded ->
                        el [] <| loadedSvg

                    Errored _ ->
                        el [] <| erroredSvg
                )
            ]

    else
        column [ height (fill |> maximum model.height), Background.color sidebarBackgroundColor, centerY, centerX, width (px 64) ] <| navElements model



-- link [ centerX, padding 10 ]
--         { url = "/"
--         , label =
--             row [ centerX, centerY, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ], pointer ]
--                 (if model.showMenu then
--                     [ oxheadSvg, el [ moveRight 5 ] <| text "Home" ]
--                  else
--                     [ withTooltip "Home" oxheadSvg ]
--                 )
--         }


navElements : Model -> List (Element Msg)
navElements model =
    [ if model.device.class == Element.Phone || model.device.class == Element.Tablet then
        Element.none

      else
        Input.button [ centerX, height (px 64), width fill, Font.color (Element.rgb255 94 96 102), Element.mouseOver [ Font.color (Element.rgb 225 225 225) ], pointer, paddingEach { bottom = 64, left = 0, right = 0, top = 64 } ]
            { onPress = Just FetchDataClicked
            , label =
                row [ centerX ]
                    (if model.showMenu then
                        [ fetchDataSvg, el [ moveRight 5 ] <| text "Fetch Data" ]

                     else
                        [ withTooltip "Fetch Data" <| fetchDataSvg ]
                    )
            }
    , if model.device.class == Element.Phone || model.device.class == Element.Tablet then
        Element.none

      else
        Input.button
            [ centerX
            , height (px 64)
            , width fill
            , Font.color (Element.rgb255 94 96 102)
            , Element.mouseOver [ Font.color (Element.rgb255 225 225 225) ]
            , pointer
            , paddingEach { bottom = 128, left = 0, right = 0, top = 0 }
            , Font.color
                (if model.isMonitoring then
                    Element.rgb255 225 225 225

                 else
                    Element.rgb255 94 96 102
                )
            ]
            { onPress = Just ToggleMonitoring
            , label =
                row [ centerX ]
                    (if model.showMenu then
                        [ el
                            (if model.isMonitoring then
                                [ Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ]

                             else
                                []
                            )
                          <|
                            monitorSvg
                        , el [] <| text "Monitor"
                        ]

                     else
                        [ withTooltip "Monitor" <|
                            el
                                (if model.isMonitoring then
                                    [ Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ]

                                 else
                                    []
                                )
                            <|
                                monitorSvg
                        ]
                    )
            }
    , link
        [ centerX
        , Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 }
        , width fill
        , height (px 64)
        , Border.color
            (if toRoute (Url.toString model.url) == Node then
                Element.rgb255 225 225 225

             else
                sidebarBackgroundColor
            )
        , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == Node then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        , pointer
        ]
        { url = "/node"
        , label =
            row
                [ centerX
                ]
                (if model.showMenu then
                    [ nodeSvg, el [ moveRight 5 ] <| text "Node" ]

                 else
                    [ withTooltip "Node" <| nodeSvg ]
                )
        }
    , link
        [ centerX
        , Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 }
        , width fill
        , height (px 64)
        , Border.color
            (if toRoute (Url.toString model.url) == Distribution then
                Element.rgb255 225 225 225

             else
                sidebarBackgroundColor
            )
        , Element.mouseOver [ Font.color (Element.rgb 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == Distribution then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        , pointer
        ]
        { url = "/distribution"
        , label =
            row
                [ centerX
                ]
                (if model.showMenu then
                    [ stakePoolsSvg, el [ moveRight 5 ] <| text "Distribution" ]

                 else
                    [ withTooltip "Distribution" <| stakePoolsSvg ]
                )
        }
    , link
        [ centerX
        , Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 }
        , width fill
        , height (px 64)
        , Border.color
            (if toRoute (Url.toString model.url) == Leaders then
                Element.rgb255 225 225 225

             else
                sidebarBackgroundColor
            )
        , Element.mouseOver [ Font.color (Element.rgb255 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == Leaders then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        , pointer
        ]
        { url = "/leaders"
        , label =
            row
                [ centerX
                ]
                (if model.showMenu then
                    [ leadersSvg, el [ moveRight 5 ] <| text "Leaders" ]

                 else
                    [ withTooltip "Leaders" <| leadersSvg ]
                )
        }
    , link
        [ centerX
        , Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 }
        , width fill
        , height (px 64)
        , Border.color
            (if toRoute (Url.toString model.url) == Settings then
                Element.rgb255 225 225 225

             else
                sidebarBackgroundColor
            )
        , Element.mouseOver [ Font.color (Element.rgb255 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == Settings then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        , pointer
        ]
        { url = "/settings"
        , label =
            row
                [ centerX
                ]
                (if model.showMenu then
                    [ settingsSvg, el [ moveRight 5 ] <| text "Settings" ]

                 else
                    [ withTooltip "Settings" <| settingsSvg ]
                )
        }
    , link
        [ centerX
        , Border.widthEach { bottom = 0, left = 2, right = 0, top = 0 }
        , width fill
        , height (px 64)
        , Border.color
            (if toRoute (Url.toString model.url) == About then
                Element.rgb255 225 225 225

             else
                sidebarBackgroundColor
            )
        , Element.mouseOver [ Font.color (Element.rgb255 225 225 225) ]
        , Font.color
            (if toRoute (Url.toString model.url) == About then
                Element.rgb255 255 255 255

             else
                Element.rgb255 94 96 102
            )
        , pointer
        ]
        { url = "/about"
        , label =
            row
                [ centerX
                ]
                (if model.showMenu then
                    [ aboutSvg, el [ moveRight 5 ] <| text "About" ]

                 else
                    [ withTooltip "About" aboutSvg ]
                )
        }
    , if model.device.class == Element.Phone || model.device.class == Element.Tablet then
        Element.none

      else
        el
            [ centerX
            , Region.footer
            , alignBottom
            , Background.color sidebarBackgroundColor
            , paddingXY 0 10
            ]
            (case model.status of
                Fetching ->
                    el [ Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ] <| fetchingSvg

                Loaded ->
                    el [] <| loadedSvg

                Errored _ ->
                    el [] <| erroredSvg
            )
    ]


webDataView : (a -> Element Msg) -> WebData a -> Element Msg
webDataView successView webData =
    case webData of
        NotAsked ->
            el [ centerX, centerY, fontMono ] <| text "Nothing to see here, fetch some data!"

        Loading ->
            el [ centerX, centerY, Element.htmlAttribute <| Html.Attributes.style "-webkit-animation" "1s spinner infinite" ] <| loadingSvg

        Failure err ->
            case err of
                Http.BadUrl url ->
                    el [ centerX, centerY, fontMono ] <| text <| "Bad URL: " ++ url

                Http.Timeout ->
                    el [ centerX, centerY, fontMono ] <| text <| "Connection Timeout"

                Http.NetworkError ->
                    el [ centerX, centerY, fontMono ] <| text <| "Couldn't Connect To Jörmungandr"

                Http.BadStatus statusCode ->
                    el [ centerX, centerY, fontMono ] <| text <| "Couldn't Retrieve Data: " ++ String.fromInt statusCode

                Http.BadBody error ->
                    el [ centerX, centerY, fontMono ] <| text <| "Bad response, please contact the developer: " ++ error

        Success data ->
            successView data


viewHome : Element Msg
viewHome =
    column [ padding 20, spacing 20 ]
        [ column [ centerX, spacing 5 ]
            [ column [ padding 20, spacing 5 ]
                [ Element.paragraph [ fontMono, Font.size 12 ] [ text "\"...Thor then prepares a strong line and a large hook and baits it with the ox head, which Jörmungandr bites.\"" ]
                , Element.paragraph [ fontMono, Font.size 12 ] [ newTabLink [ alignRight, Font.color blue ] { url = "https://en.wikipedia.org/wiki/J%C3%B6rmungandr", label = text "Wikipedia" } ]
                ]
            , el [ Font.color (Element.rgb255 0 0 0), centerX, width (px 180), Font.center ] oxheadSvgColored
            , Element.paragraph [ fontNormal, Font.size 28, centerX, Font.center ] [ text "Welcome to Ox Head" ]
            , Element.paragraph [ fontMono, Font.size 16, centerX ] [ text "A lightweight dashboard and control system for your Jörmungandr Cardano node." ]
            ]
        , column [ paddingXY 120 60, spacing 20 ]
            [ Element.paragraph []
                [ text "Ox Head is an static page that works locally on your computer. It allows you to see useful information from your Jörmungandr Cardano Node by making request to the node's"
                , el [ Font.bold ] (text " REST API ")
                , text "for analysis and monitoring. "
                , text "Just put your node address on the "
                , link [ Font.color blue ]
                    { url = "/settings"
                    , label = text "Settings"
                    }
                , text " section and start using it!"
                ]
            , Element.column [ spacing 10, padding 10 ]
                [ Element.paragraph [ Font.bold ] [ text "Take Note" ]
                , Element.paragraph [] [ text "If your node rest api is served through http you have to do at least one of the following: " ]
                , column [ padding 10, spacing 5 ]
                    [ Element.paragraph [] [ el [ Font.bold ] (text "• "), text "Allow mixed contents in your browser." ]
                    , Element.paragraph []
                        [ el [ Font.bold ] (text "• ")
                        , text "Make your api comunicate through https with tools such as "
                        , newTabLink [ Font.color blue, fontMono ]
                            { url = "https://www.stunnel.org/"
                            , label = text "stunnel."
                            }
                        ]
                    ]
                , Element.paragraph [] [ text "If your node rest api is served on a remote server: " ]
                , column [ padding 10, spacing 5 ]
                    [ Element.paragraph [] [ el [ Font.bold ] (text "• "), text "Make sure you can reach the node rest api safely." ]
                    , Element.paragraph []
                        [ el [ Font.bold ] (text "• ")
                        , text "You could achieve that by creating an SSH Tunnel between the client (where you open oxhead) and the remote node."
                        ]
                    ]
                ]
            ]
        ]


merge3 :
    RemoteData e a
    -> RemoteData e b
    -> RemoteData e c
    -> RemoteData e ( a, b, c )
merge3 a b c =
    RemoteData.map (\x y z -> ( x, y, z )) a
        |> RemoteData.andMap b
        |> RemoteData.andMap c


viewNode : Model -> ( NodeStats, NodeSettings, List NetworkStats ) -> Element Msg
viewNode model ( nodeStats, nodeSettings, networkStats ) =
    column
        [ Font.size 14
        , fontMono
        , spacing 15
        , padding 10
        , width fill
        ]
        [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 32, Font.extraBold ] <| text <| "Node"
            ]
        , wrappedRow
            [ width fill, spacing 10 ]
            [ viewNodeStats model.showNodeStats model.zone nodeSettings.slotsPerEpoch nodeStats
            , viewNodeSettings model.showNodeSettings model.zone nodeSettings
            ]
        , viewNetworkStats model.showNetworkStats model.zone networkStats
        ]



-- , case model.utxoList of
--     Just stats ->
--         viewPanel (viewUTXOList stats)
--     Nothing ->
--         Element.none
-- , case model.tip of
--     Just tip ->
--         el [] <| text tip
--     Nothing ->
--         Element.none
-- , case model.stakePoolsList of
--     Just stakePoolsList ->
--         viewPanel (viewStakePools stakePoolsList)
--     Nothing ->
--         Element.none
-- , case model.stakeDistribution of
--     Just stakeDistribution ->
--         viewPanel (viewStakeDistribution stakeDistribution)
--     Nothing ->
--         Element.none
-- , case model.leaders of
--     Just leadersList ->
--         viewPanel (viewLeaders leadersList)
--     Nothing ->
--         Element.none
-- , case model.leadersLogs of
--     Just leadersLogsList ->
--         viewPanel (viewLeadersLogs leadersLogsList)
--     Nothing ->
--         Element.none
-- , case model.fragmentLogs of
--     Just fragmentLogsList ->
--         viewPanel (viewFragmentLogs fragmentLogsList)
--     Nothing ->
--         Element.none
-- , case model.account of
--     Just acc ->
--         viewPanel (viewAccount acc)
--     Nothing ->
--         Element.none


viewNodeStats : Bool -> Time.Zone -> Int -> NodeStats -> Element Msg
viewNodeStats showNodeStats zone slotsPerEpoch nodeStats =
    case nodeStats of
        Initializing stats ->
            column
                [ padding 10
                , spacing 5
                , alignTop
                , width fill
                , fontMono
                , Background.color (Element.rgb255 255 214 68)
                ]
                [ row [ width fill, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, paddingXY 2 10 ]
                    [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Node Status"
                    ]
                , column
                    [ width fill, spacing 10, padding 10 ]
                    [ wrappedRow [ width fill ]
                        [ el [ alignLeft ] (withTooltip "Node app version" <| text <| "Version")
                        , el [ alignRight ] <| text <| stats.version
                        ]
                    , wrappedRow [ width fill ]
                        [ el [ alignLeft ] (withTooltip "State of the node" <| text <| "State")
                        , el [ alignRight ] <| text <| stats.state
                        ]
                    ]
                ]

        Running stats ->
            column
                [ Border.color (Element.rgb255 198 205 214)
                , Border.width 2
                , padding 10
                , spacing 5
                , alignTop
                , width fill
                ]
                (viewRunningNodeStats stats showNodeStats zone slotsPerEpoch)


viewDistribution : Model -> Element Msg
viewDistribution model =
    Lazy.lazy
        (webDataView
            (viewStakeDistribution
                model.stakePoolDetail
                model.stakePoolID
                model.myStakePoolID
                model.showZeroStaked
                model.sortAscending
                model.showStakePoolInputSuggestions
            )
        )
        model.stakeDistribution



-- , case model.leaders of
--     Just leadersList ->
--         viewPanel (viewLeaders leadersList)
--     Nothing ->
--         Element.none
-- , case model.leadersLogs of
--     Just leadersLogsList ->
--         viewPanel (viewLeadersLogs leadersLogsList)
--     Nothing ->
--         Element.none
-- , case model.fragmentLogs of
--     Just fragmentLogsList ->
--         viewPanel (viewFragmentLogs fragmentLogsList)
--     Nothing ->
--         Element.none


viewSettings : Model -> Element Msg
viewSettings model =
    column [ spacing 20, padding 20 ]
        [ column [ spacing 20, padding 20 ]
            [ Input.text [ centerY, width (px 300), fontMono, spacing 20 ] { onChange = NodeURLChanged, text = model.nodeUrl, placeholder = Maybe.Just (Input.placeholder [ centerY, fontMono ] (text "Insert Node URL")), label = Input.labelAbove [ centerY, fontNormal ] (text "Set your Node REST API Listening Address:") }
            , Input.text [ centerY, width (px 300), fontMono, spacing 20 ] { onChange = MyStakePoolIDChanged, text = model.myStakePoolID, placeholder = Maybe.Just (Input.placeholder [ centerY, fontMono ] (text "Insert Your Stake Pool ID")), label = Input.labelAbove [ centerY, fontNormal ] (text "Set your Stake Pool ID:") }
            ]
        , column [ spacing 20, padding 20 ]
            [ el [] (text "In order to work, this domain should be addded to the CORS section of your node configuration: ")
            , el [ fontMono, Font.size 12 ] (text " \"rest\": { \"listen\": \"127.0.0.1:3100\", \"cors\": { \"allowed_origins\": [\"https://oxhead.info\"]}}")
            , el [] (text "You may also require to allow mixed contents on your browser")
            , image
                []
                { src = "./assets/disableProtection.png", description = "Disable Protection" }
            ]
        ]


viewAbout : Element Msg
viewAbout =
    column [ centerX, centerY, spacing 10, padding 10 ]
        [ el [ Font.color (Element.rgb255 0 0 0), centerX, Font.center ] oxheadSvgColored
        , el [ centerX, Region.heading 1, Font.size 36, fontMono, Font.center ] (text "OX HEAD")
        , el [ centerX, Region.heading 2, Font.size 28, fontMono ] (text "Dashboard and control system for your Cardano node")
        , row [ centerX, spacing 2 ]
            [ el [ centerX, fontMono ] (text "Made with ")
            , newTabLink [ Font.color blue, fontMono ]
                { url = "https://elm-lang.org"
                , label = elmLogoSvg
                }
            , el [] (text " and <3 by ")
            , newTabLink [ Font.color blue, fontMono ]
                { url = "https://twitter.com/arguser"
                , label = text "Leo"
                }
            ]
        , row [ centerX, spacing 5, padding 20 ]
            [ newTabLink [ Font.color blue, fontMono ]
                { url = "https://www.cardano.org/en/home/"
                , label = text "Cardano"
                }
            , newTabLink [ Font.color blue, fontMono ]
                { url = "https://github.com/input-output-hk/jormungandr"
                , label = text "Jörmungandr"
                }
            ]
        , row [ centerX, spacing 5, padding 20 ]
            [ el [] (text "Hosted on")
            , newTabLink [ Font.color blue, fontMono ]
                { url = "https://gitlab.com/arguser/oxhead"
                , label = row [] [ text "GitLab ", gitlabLogoSvg ]
                }
            ]
        , column [ alignBottom, centerX, spacing 5, padding 20 ]
            [ el [ centerX ] (text "Support")
            , newTabLink [ Font.color blue, fontMono ]
                { url = "https://seiza.com/blockchain/address/Ae2tdPwUPEZHc31dY71ehfqF69JDkgi9CN3Ka2rQi9ZPsKiTyYbeZ7MYyZc"
                , label = row [ spacing 5 ] [ adaIcon, text "Ae2tdPwUPEZHc31dY71ehfqF69JDkgi9CN3Ka2rQi9ZPsKiTyYbeZ7MYyZc" ]
                }
            ]
        ]


ratioToText : Ratio -> Element Msg
ratioToText ratio =
    let
        calculateRatio =
            toFloat ratio.denominator / toFloat ratio.numerator

        ratioString =
            String.fromFloat <| calculateRatio * 100
    in
    text ratioString


parseBlockDate : String -> ( Float, Float )
parseBlockDate blockDateString =
    let
        splitList =
            String.split "." blockDateString

        firstElement =
            Maybe.withDefault 0 <| String.toFloat <| Maybe.withDefault "0" (List.head splitList)

        secondElement =
            Maybe.withDefault 0 <| String.toFloat <| Maybe.withDefault "0" (List.head (Maybe.withDefault [] (List.tail splitList)))
    in
    Tuple.pair firstElement secondElement


viewRunningNodeStats : RunningNodeStats -> Bool -> Time.Zone -> Int -> List (Element Msg)
viewRunningNodeStats nodeStats showNodeStats timeZone slotsPerEpoch =
    [ row [ width fill, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, paddingXY 2 10 ]
        [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Node Status"
        , Input.button [ alignRight ] { onPress = Just ToggleNodeStatsPanel, label = toggleIcon showNodeStats }
        ]
    , if showNodeStats then
        wrappedRow []
            [ el [ centerX ] <| Element.html <| viewEpoch (Tuple.second <| parseBlockDate nodeStats.lastBlockDate) (toFloat slotsPerEpoch)
            , column [ width fill, spacing 10, padding 10 ]
                [ wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Node app version" <| text <| "Version")
                    , el [ alignRight ] <| text <| nodeStats.version
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Size in bytes of all transactions in last block" <| text <| "Last Block Content")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockContentSize
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Number of blocks received by node" <| text <| "Blocks Recieved")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.blockRecvCnt
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "The Epoch and slot Number of the block" <| text <| "Last Block Date")
                    , el [ alignRight ] <| text <| nodeStats.lastBlockDate
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Sum of all fee values in all transactions in last block" <| text <| "Last Block Fees")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockFees
                    ]
                , wrappedRow [ width fill, spacing 5 ]
                    [ el [ alignLeft ] (withTooltip "The block hash, it's unique identifier in the blockchain" <| text <| "Last Block Hash")
                    , el [ alignRight ] <| text <| nodeStats.lastBlockHash
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "The block number, in order, since the block0" <| text <| "Last Block Height")
                    , el [ alignRight ] <| text <| nodeStats.lastBlockHeight
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Sum of all input values in all transactions in last block" <| text <| "Last Block Sum")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockSum
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "When last block was created, not set if none was created yet" <| text <| "Last Block Time")
                    , el [ alignRight ] <| text <| maybeTimeToDateString nodeStats.lastBlockTime timeZone
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Number of transactions in last block" <| text <| "Last Block Tx")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.lastBlockTx
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "State of the node" <| text <| "State")
                    , el [ alignRight ] <| text <| nodeStats.state
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Number of transactions received by node" <| text <| "TX Received Count")
                    , el [ alignRight ] <| text <| String.fromInt nodeStats.txRecvCnt
                    ]
                , wrappedRow [ width fill ]
                    [ el [ alignLeft ] (withTooltip "Node uptime" <| text <| "Uptime since")
                    , el [ alignRight ] (withTooltip (String.fromInt (Time.posixToMillis nodeStats.uptime // 1000)) <| text <| RelativeDateFormat.relativeTime nodeStats.uptime (Time.millisToPosix 0))
                    ]
                ]
            ]

      else
        Element.none
    ]


viewNetworkStats : Bool -> Time.Zone -> List NetworkStats -> Element Msg
viewNetworkStats showNetworkStats timeZone networkStats =
    column
        [ Border.color (Element.rgb255 198 205 214)
        , Border.width 2
        , padding 10
        , spacing 5
        , alignTop
        , width fill
        ]
        [ column [ width fill, spacing 10 ]
            [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
                [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Network Status"
                , Input.button toggleButtonStyle { onPress = Just ToggleNetworkStatsPanel, label = toggleIcon showNetworkStats }
                ]
            , row [ fontNormal, Font.size 22, width fill, Font.bold ]
                [ el [ alignLeft ] <| text <| "Connections"
                , el [ alignRight ] <| text <| String.fromInt (List.length networkStats)
                ]
            ]
        , if showNetworkStats then
            column
                [ width fill
                , spacing 10
                , padding 10
                , scrollbarY
                , height (fill |> maximum 400)
                ]
                [ Element.indexedTable [] { data = networkStats, columns = networkStatsColumns timeZone } ]

          else
            Element.none
        ]


networkStatsColumns : Time.Zone -> List (IndexedColumn NetworkStats Msg)
networkStatsColumns timeZone =
    [ { header = el [ fontNormal, Font.size 24, padding 10 ] <| text <| "Node IP Address"
      , width = fill
      , view =
            \index stats ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| stats.addr ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| text <| "Established At"
      , width = fill
      , view =
            \index stats ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| timeToDateString stats.establishedAt timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| text <| "Last Block Received"
      , width = fill
      , view =
            \index stats ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| maybeTimeToDateString stats.lastBlockReceived timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| text <| "Last Fragment Received"
      , width = fill
      , view =
            \index stats ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| maybeTimeToDateString stats.lastFragmentReceived timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| text <| "Last Gossip Received"
      , width = fill
      , view =
            \index stats ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| maybeTimeToDateString stats.lastGossipReceived timeZone ]
      }
    ]



--             , wrappedRow [ width fill ]
--                 [ withTooltip "Timestamp of last time block was received from node if ever" <| el [ alignLeft ] <| text <| "Last Block Received"
--                 , el [ alignRight ] <| text <| maybeTimeToDateString stats.lastBlockReceived timeZone
--                 ]
--             , wrappedRow [ width fill ]
--                 [ withTooltip "Timestamp of last time fragment was received from node if ever" <| el [ alignLeft ] <| text <| "Last Fragment Received"
--                 , el [ alignRight ] <| text <| maybeTimeToDateString stats.lastFragmentReceived timeZone
--                 ]
--             , wrappedRow [ width fill ]
--                 [ withTooltip "Timestamp of last time gossip was received from node if ever" <| el [ alignLeft ] <| text <| "Last Fragment Received"
--                 , el [ alignRight ] <| text <| maybeTimeToDateString stats.lastGossipReceived timeZone
--                 ]
--             ]
--     )
--     networkStats
-- )


viewNodeSettings : Bool -> Time.Zone -> NodeSettings -> Element Msg
viewNodeSettings showNodeSettings timeZone nodeSettings =
    column
        [ Border.color (Element.rgb255 198 205 214)
        , Border.width 2
        , padding 10
        , spacing 5
        , alignTop
        , width fill
        ]
        [ row [ width fill, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, paddingXY 2 10 ]
            [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Node Settings"
            , Input.button toggleButtonStyle { onPress = Just ToggleNodeSettingsPanel, label = toggleIcon showNodeSettings }
            ]
        , if showNodeSettings then
            column [ width fill, spacing 10, padding 10 ]
                [ wrappedRow [ width fill ]
                    [ withTooltip "Hex-encoded hash of block0" <| el [ alignLeft ] <| text <| "Blocks Hash"
                    , el [ alignRight ] <| text <| nodeSettings.block0Hash
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "When block0 was created" <| el [ alignLeft ] <| text <| "Block Time"
                    , el [ alignRight ] <| text <| timeToDateString nodeSettings.block0Time timeZone
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "The block content's max size in bytes" <| el [ alignLeft ] <| text <| "Max Block Content"
                    , el [ alignRight ] <| text <| String.fromInt nodeSettings.blockContentMaxSize
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "Version of consensus, which is currently used" <| el [ alignLeft ] <| text <| "Consensus Version"
                    , el [ alignRight ] <| text <| nodeSettings.consensusVersion
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "When current slot was opened, not set if none is currently open" <| el [ alignLeft ] <| text <| "Current Slot Start Time"
                    , el [ alignRight ] <| text <| maybeTimeToDateString nodeSettings.currSlotStartTime timeZone
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "The depth, number of blocks, to which we consider the blockchain to be stable and prevent rollback beyond that depth" <| el [ alignLeft ] <| text <| "Stability Depth"
                    , el [ alignRight ] <| text <| String.fromInt nodeSettings.epochStabilityDepth
                    ]
                , column [ width fill ]
                    [ withTooltip "Linear fees configuration" <| el [ alignLeft ] <| text <| "Fees"
                    , column [ width fill, spacing 10, padding 10 ] (viewNodeSettingsFees nodeSettings.fees)
                    ]
                , column [ width fill ]
                    [ withTooltip "Parameters for rewards calculation" <| el [ alignLeft ] <| text <| "Rewards Parameters"
                    , column [ width fill, spacing 10, padding 10 ] (viewNodeSettingsRewardParams nodeSettings.rewardParams)
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "Slot duration in seconds" <| el [ alignLeft ] <| text <| "Slot Duration"
                    , el [ alignRight ] <| text <| String.fromInt nodeSettings.slotDuration
                    ]
                , wrappedRow [ width fill ]
                    [ withTooltip "Number of slots per epoch" <| el [ alignLeft ] <| text <| "Slots Per Epoch"
                    , el [ alignRight ] <| text <| String.fromInt nodeSettings.slotsPerEpoch
                    ]
                , column [ width fill ]
                    [ withTooltip "Tax from reward that goes to pot" <| el [ alignLeft ] <| text <| "Treasury Tax"
                    , column [ width fill, spacing 10, padding 10 ] (viewNodeSettingsTreasuryTax nodeSettings.treasuryTax)
                    ]
                ]

          else
            Element.none
        ]


viewNodeSettingsFees : NodeSettingsFees -> List (Element Msg)
viewNodeSettingsFees nodeSettingsFees =
    [ row [ width fill ]
        [ withTooltip "Fee per certificate used in witness" <| el [ alignLeft ] <| text <| "Certificate"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue nodeSettingsFees.certificate ]
        ]
    , row [ width fill ]
        [ withTooltip "Fee per every input and output of transaction" <| el [ alignLeft ] <| text <| "Coefficient"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue nodeSettingsFees.coefficient ]
        ]
    , row [ width fill ]
        [ withTooltip "Base fee per transaction" <| el [ alignLeft ] <| text <| "Constant"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue nodeSettingsFees.constant ]
        ]
    ]


viewNodeSettingsRewardParams : NodeSettingsRewardParams -> List (Element Msg)
viewNodeSettingsRewardParams nodeSettingsRewardParams =
    [ row [ width fill ]
        [ withTooltip "Speed at which reward is reduced. Expressed as numerator/denominator" <| el [ alignLeft ] <| text <| "Compounding Ratio"
        , row [ alignRight ] [ ratioToText nodeSettingsRewardParams.compoundingRatio, text "%" ]
        ]
    , row [ width fill ]
        [ withTooltip "Reward reduction algorithm" <| el [ alignLeft ] <| text <| "Compounding Type"
        , el [ alignRight ] <| text <| nodeSettingsRewardParams.compoundingType
        ]
    , row [ width fill ]
        [ withTooltip "Number of epochs between reward" <| el [ alignLeft ] <| text <| "Epoch Rate"
        , el [ alignRight ] <| text <| String.fromInt nodeSettingsRewardParams.epochRate
        ]
    , row [ width fill ]
        [ withTooltip "Epoch when rewarding starts" <| el [ alignLeft ] <| text <| "Epoch Start"
        , el [ alignRight ] <| text <| String.fromInt nodeSettingsRewardParams.epochStart
        ]
    , row [ width fill ]
        [ withTooltip "Initial reward" <| el [ alignLeft ] <| text <| "Initial Value"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue nodeSettingsRewardParams.initialValue ]
        ]
    ]


viewNodeSettingsTreasuryTax : NodeSettingsTreasuryTax -> List (Element Msg)
viewNodeSettingsTreasuryTax nodeSettingsTreasuryTax =
    [ row [ width fill ]
        [ withTooltip "What get subtracted as fixed value" <| el [ alignLeft ] <| text <| "Fixed Value"
        , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| String.fromInt nodeSettingsTreasuryTax.fixed ]
        ]
    , row [ width fill ]
        [ withTooltip "Ratio of tax after fixed amount is subtracted" <| el [ alignLeft ] <| text <| "Ratio"
        , row [ alignRight ] [ ratioToText nodeSettingsTreasuryTax.ratio, text "%" ]
        ]
    ]



-- viewUTXOList : List UTXO -> List (Element Msg)
-- viewUTXOList utxoList =
--     List.map
--         (\utxo ->
--             column []
--                 [ row []
--                     [ el [ alignLeft ] <| text <| "Transaction ID"
--                     , el [ alignRight ] <| text <| utxo.transaction_id
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Index In Transaction ID"
--                     , el [ alignRight ] <| text <| String.fromInt utxo.index_in_transaction
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Address"
--                     , el [ alignRight ] <| text <| utxo.address
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Associated Fund"
--                     , el [ alignRight ] <| text <| String.fromInt utxo.associated_fund
--                     ]
--                 ]
--         )
--         utxoList
-- viewStakePools : List String -> Bool -> List (Element Msg)
-- viewStakePools stakePoolsList showStakePools =
--     [ column [ width fill, spacing 10 ]
--         [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
--             [ el [ alignLeft, fontNormal, Font.size 24 ] <| text <| "Stake Pools List"
--             , Input.button toggleButtonStyle { onPress = Just ToggleStakePoolsPanel, label = toggleIcon showStakePools }
--             ]
--         , row [ fontNormal, Font.size 16, width fill ]
--             [ el [ alignLeft ] <| text <| "Total Stake Pools"
--             , el [ alignRight ] <| text <| String.fromInt (List.length stakePoolsList)
--             ]
--         ]
--     , if showStakePools then
--         column [ width fill, spacing 10, padding 10 ]
--             (viewStakePoolsSimple stakePoolsList)
--       else
--         Element.none
--     ]
-- viewStakePoolsSimple : List String -> List (Element Msg)
-- viewStakePoolsSimple stakePoolsList =
--     List.map
--         (\stakePool ->
--             el [ spacing 5 ] <| text <| stakePool
--         )
--         stakePoolsList


viewStakeDistribution : WebData StakePool -> String -> String -> Bool -> Bool -> Bool -> StakeDistribution -> Element Msg
viewStakeDistribution stakePoolDetail stakePoolID myStakePoolID showZeroStaked sortDescending showSuggestions stakeDistribution =
    let
        stakeDetail =
            stakeDistribution.stake

        stakedValue =
            List.foldl (+) 0 (List.map (\pool -> pool.value) stakeDetail.pools)

        totalValue =
            stakeDetail.dangling + stakeDetail.unassigned + List.foldl (+) 0 (List.map (\pool -> pool.value) stakeDetail.pools)

        stakedPools =
            List.length (List.filter (\sp -> sp.value > 0) stakeDetail.pools)

        totalPools =
            List.length stakeDetail.pools

        cardanoExplorerLink =
            if String.length stakePoolID == 64 then
                newTabLink [ Font.color blue, fontMono ]
                    { url = "https://shelleyexplorer.cardano.org/en/stake-pool/" ++ stakePoolID, label = text "See on Explorer" }

            else
                withTooltip "Invalid Stake Pool ID Length" <| el [ width fill ] <| text "See on Explorer"
    in
    row
        [ Font.size 14
        , fontMono
        , spacing 15
        , padding 10
        , width fill
        ]
        -- [ case model.stakePoolsList of
        --     Just stakePoolsList ->
        --         viewPanel (viewStakePools stakePoolsList model.showStakePoolsList)
        --     Nothing ->
        --         Element.none
        [ column [ centerX, width fill ]
            [ column [ width fill, spacing 10 ]
                [ row [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10 ]
                    [ el [ alignLeft, fontNormal, Font.size 32, Font.extraBold ] <| text <| "Stake Distribution"
                    ]
                ]
            , wrappedRow [ width fill, padding 10, spacing 30 ]
                [ column [ width (fillPortion 2), spacing 10, padding 10, alignTop, fontMono, Border.widthEach { bottom = 0, left = 0, right = 1, top = 0 }, Font.size 14 ]
                    [ row [ width fill ]
                        [ el [ alignLeft ] <| text <| "Stake Pools Staking"
                        , el [ alignRight ] <| text <| String.fromInt stakedPools
                        ]
                    , row [ width fill ]
                        [ el [ alignLeft ] <| text <| "Total Stake Pools"
                        , el [ alignRight ] <| text <| String.fromInt totalPools
                        ]
                    , column [ width fill, spacing 10 ]
                        [ row [ width fill ]
                            [ withTooltip "Epoch of last block" <| el [ alignLeft ] <| text <| "Epoch"
                            , el [ alignRight ] <| text <| String.fromInt stakeDistribution.epoch
                            ]
                        , column [ width fill, spacing 10 ]
                            [ row [ width fill ]
                                [ withTooltip "Total value stored in accounts, but assigned to nonexistent pools" <| el [ alignLeft ] <| text <| "Dangling"
                                , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue stakeDetail.dangling ]
                                ]
                            , row [ width fill ]
                                [ withTooltip "Total value stored in accounts, but not assigned to any pool" <| el [ alignLeft ] <| text <| "Unassigned"
                                , row [ alignRight, spacing 5 ] [ adaIcon, el [ alignRight ] <| text <| formatAdaValue stakeDetail.unassigned ]
                                ]
                            , row [ width fill ]
                                [ withTooltip "Total value staked between pools" <| el [ width fill, alignLeft ] <| text <| "Total Staked"
                                , row [ alignRight, spacing 5 ] [ adaIcon, el [] <| text <| formatAdaValue stakedValue ]
                                ]
                            , row [ width fill ]
                                [ withTooltip "Total value in circulation" <| el [ width fill, alignLeft ] <| text <| "Total"
                                , row [ alignRight, spacing 5 ] [ adaIcon, el [] <| text <| formatAdaValue totalValue ]
                                ]
                            ]
                        ]
                    , column [ Border.widthEach { bottom = 0, left = 0, right = 0, top = 1 }, width fill, paddingEach { bottom = 0, left = 0, right = 0, top = 10 }, spacing 10 ]
                        [ if showSuggestions then
                            row [ width fill, spacing 5 ]
                                [ Input.search [ centerY, Events.onFocus ActivateStakePoolInput, Events.onLoseFocus DeactivateStakePoolInput, inputSuggestions stakePoolID (List.map (\pool -> pool.id) stakeDistribution.stake.pools) StakePoolIdClicked ]
                                    { onChange = StakePoolIdChanged
                                    , text = stakePoolID
                                    , placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Click a Stake Pool ID"))
                                    , label = Input.labelLeft [ centerY ] (text "Stake Pool ID: ")
                                    }
                                , cardanoExplorerLink
                                ]

                          else
                            row [ width fill, spacing 5 ]
                                [ Input.search [ centerY, Events.onFocus ActivateStakePoolInput, Events.onLoseFocus DeactivateStakePoolInput ]
                                    { onChange = StakePoolIdChanged
                                    , text = stakePoolID
                                    , placeholder = Maybe.Just (Input.placeholder [ centerY ] (text "Click a Stake Pool ID"))
                                    , label = Input.labelLeft [ centerY ] (text "Stake Pool ID: ")
                                    }
                                , cardanoExplorerLink
                                ]
                        , case stakePoolDetail of
                            NotAsked ->
                                el [ centerX ] <| text <| "Click a Stake Pool ID to see details "

                            _ ->
                                webDataView viewStakePoolDetail stakePoolDetail
                        ]
                    ]
                , viewStakePoolsTable stakeDistribution.stake.pools showZeroStaked sortDescending myStakePoolID stakedValue
                ]
            ]
        ]


inputSuggestions : String -> List String -> (String -> Msg) -> Element.Attribute Msg
inputSuggestions inputText suggestionLists suggestionClickedMsg =
    Element.below <|
        el
            [ height
                (fill
                    |> minimum 300
                    |> maximum 600
                )
            , width fill
            ]
        <|
            column
                [ Border.shadow
                    { offset = ( 3, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
                , scrollbarY
                , width fill
                ]
                (let
                    filteredSuggestions =
                        List.filter (\suggestionItem -> String.startsWith inputText suggestionItem) suggestionLists
                 in
                 List.map
                    (\suggestion ->
                        el [ Background.color (Element.rgb255 255 255 255), Border.width 1, Border.color (Element.rgb255 198 205 214), width fill ] <| Input.button [ padding 10, spacing 5, width fill, Events.onMouseDown (suggestionClickedMsg suggestion) ] { onPress = Nothing, label = Element.text <| suggestion }
                    )
                    filteredSuggestions
                )


viewStakePoolDetail : StakePool -> Element Msg
viewStakePoolDetail stakePool =
    column [ width fill, spacing 10 ]
        [ wrappedRow [ width fill ]
            [ withTooltip "Bech32-encoded stake pool KES key" <| el [ alignLeft ] <| text <| "KES Public Key"
            , el [ alignRight ] <| text <| stakePool.kesPublicKey
            ]
        , column [ width fill ]
            [ withTooltip "Reward received by the stake pool during the associated epoch" <| el [ alignLeft ] <| text <| "Rewards"
            , viewStakePoolRewards stakePool.rewards
            ]
        , column [ width fill ]
            [ withTooltip "Pool reward" <| el [ alignLeft ] <| text <| "Tax"
            , viewStakePoolTax stakePool.tax
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Total stake pool value" <| el [ alignLeft ] <| text <| "Total Stake"
            , row [ spacing 5, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePool.total_stake ]
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Bech32-encoded stake pool VRF key" <| el [ alignLeft ] <| text <| "VRF Public Key"
            , el [ alignRight ] <| text <| stakePool.vrfPublicKey
            ]
        ]


viewStakePoolRewards : StakePoolRewards -> Element Msg
viewStakePoolRewards stakePoolRewards =
    column [ width fill, spacing 10, padding 10 ]
        [ wrappedRow [ width fill ]
            [ withTooltip "Epoch the reward has been distributed to this stake pool for the last time" <| el [ alignLeft ] <| text <| "Epoch"
            , el [ alignRight ] <| text <| String.fromInt stakePoolRewards.epoch
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Value remaining to distribute amongst the delegators to this stake pool" <| el [ alignLeft ] <| text <| "Value For Stakers"
            , row [ spacing 5, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePoolRewards.value_for_stakers ]
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Total value taxed for the stake pool" <| el [ alignLeft ] <| text <| "Value Taxed"
            , row [ spacing 5, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePoolRewards.value_taxed ]
            ]
        ]


viewStakePoolTax : StakePoolTax -> Element Msg
viewStakePoolTax stakePoolTax =
    column [ width fill, spacing 10, padding 10 ]
        [ wrappedRow [ width fill ]
            [ withTooltip "What get subtracted as fixed value" <| el [ alignLeft ] <| text <| "Fixed"
            , row [ spacing 5, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePoolTax.fixed ]
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Ratio of tax after fixed amount is subtracted" <| el [ alignLeft ] <| text <| "Ratio"
            , row [ alignRight ] [ ratioToText stakePoolTax.ratio, text "%" ]
            ]
        , wrappedRow [ width fill ]
            [ withTooltip "Limit of tax" <| el [ alignLeft ] <| text <| "Max"
            , row [ spacing 5, alignRight ] [ adaIcon, Element.text <| formatAdaValue stakePoolTax.max ]
            ]
        ]



-- viewStakePoolTaxRatio : Ratio -> Element Msg
-- viewStakePoolTaxRatio stakePoolTaxRatio =
--     column [ width fill, spacing 10, padding 10 ]
--         [ wrappedRow [ width fill ]
--             [ el [ alignLeft ] <| text <| "Denominator"
--             , el [ alignRight ] <| text <| String.fromInt stakePoolTaxRatio.denominator
--             ]
--         , wrappedRow [ width fill ]
--             [ el [ alignLeft ] <| text <| "Numerator"
--             , el [ alignRight ] <| text <| String.fromInt stakePoolTaxRatio.numerator
--             ]
--         ]


viewStakePoolsTable : List Pool -> Bool -> Bool -> String -> Int -> Element Msg
viewStakePoolsTable stakePoolsList showZeroStaked sortAscending myStakePoolID stakedValue =
    let
        stakePools =
            if showZeroStaked then
                stakePoolsList

            else
                List.filter (\stakePool -> stakePool.value > 0) stakePoolsList

        stakePoolsSorted =
            if sortAscending then
                List.sortBy .value stakePools

            else
                List.reverse <| List.sortBy .value stakePools
    in
    column
        [ width (fillPortion 2)
        , Font.size 14
        , onLeft
            (column [ alignRight, spacing 10, width fill, padding 5 ]
                [ Input.button toggleButtonStyle { onPress = Just ToggleShowZeroStaked, label = withTooltip "Show/Hide Empty Pools" <| toggleShowHideIcon showZeroStaked }
                , Input.button toggleButtonStyle { onPress = Just ToggleSortAscending, label = withTooltip "Sort by Value" <| toggleSortingIcon sortAscending }
                ]
            )
        ]
        [ Element.table
            [ spacing 10
            , width fill
            , scrollbarY
            , height
                (fill
                    |> maximum 860
                )
            , fontMono
            ]
            { data = stakePoolsSorted
            , columns =
                [ { header = Element.text "Pool Node ID"
                  , width = fillPortion 4
                  , view =
                        \stakePool ->
                            let
                                poolSizeFontStyle =
                                    if stakePool.id == myStakePoolID then
                                        Font.bold

                                    else
                                        Font.regular
                            in
                            el [] <|
                                Input.button []
                                    { onPress = Just (StakePoolIdClicked stakePool.id)
                                    , label =
                                        el [ poolSizeFontStyle ] <| text stakePool.id
                                    }
                  }
                , { header = Element.text "Pool Size"
                  , width = fill
                  , view =
                        \stakePool ->
                            let
                                poolSize =
                                    toFloat stakePool.value * 100 / toFloat stakedValue

                                poolSizeString =
                                    if poolSize > 0.01 then
                                        (String.left 4 <| String.fromFloat <| poolSize) ++ "%"

                                    else
                                        "0.00%"

                                poolSizeStyle =
                                    if poolSize >= 1 then
                                        onRight <| erroredSvg

                                    else
                                        onLeft Element.none

                                poolSizeFontStyle =
                                    if stakePool.id == myStakePoolID then
                                        Font.bold

                                    else
                                        Font.regular
                            in
                            row [ spacing 5, width fill, alignRight ]
                                [ el [ poolSizeFontStyle, poolSizeStyle ] <| text <| poolSizeString
                                ]
                  }
                , { header = Element.text "Stake"
                  , width = fill
                  , view =
                        \stakePool ->
                            let
                                poolSizeFontStyle =
                                    if stakePool.id == myStakePoolID then
                                        Font.bold

                                    else
                                        Font.regular
                            in
                            row [ spacing 5, width fill, alignRight ]
                                [ adaIcon
                                , el [ poolSizeFontStyle ] <| text <| formatAdaValue stakePool.value
                                ]
                  }
                ]
            }
        ]



-- viewStakePoolsDetails : List ( String, Int ) -> List (Element Msg)
-- viewStakePoolsDetails stakePoolsList =
--     List.map
--         (\stakePool ->
--             column []
--                 [ el [] <| text <| Tuple.first stakePool
--                 , el [ alignRight ] <| text <| String.fromInt <| Tuple.second stakePool
--                 ]
--         )
--         stakePoolsList
-- viewLeaders : List Int -> List (Element Msg)
-- viewLeaders leadersList =
--     List.map
--         (\leader ->
--             el [] <| text <| String.fromInt leader
--         )
--         leadersList


viewLeadersLogs : Int -> Posix -> Time.Zone -> List LeadersLogs -> Element Msg
viewLeadersLogs maxHeight time timeZone leadersLogsList =
    let
        pendingLeaders =
            List.filter
                (\leader ->
                    case leader.status of
                        Pending _ ->
                            True

                        _ ->
                            False
                )
                leadersLogsList

        pendingLeadersSorted =
            List.sortBy (\leader -> Time.posixToMillis leader.scheduled_at_time) pendingLeaders

        rejectedLeaders =
            List.filter
                (\leader ->
                    case leader.status of
                        Rejected _ ->
                            True

                        _ ->
                            False
                )
                leadersLogsList

        rejectedLeadersSorted =
            List.sortBy (\leader -> Time.posixToMillis leader.scheduled_at_time) rejectedLeaders

        createdLeaders =
            List.filter
                (\leader ->
                    case leader.status of
                        Created _ ->
                            True

                        _ ->
                            False
                )
                leadersLogsList

        createdLeadersSorted =
            List.reverse <| List.sortBy (\leader -> Time.posixToMillis leader.scheduled_at_time) createdLeaders

        closestSchedule =
            case List.head <| pendingLeadersSorted of
                Just leader ->
                    leader.scheduled_at_time

                _ ->
                    time

        nonEmptyLists =
            List.length <| List.filter (\x -> not x) <| List.map List.isEmpty [ createdLeaders, rejectedLeaders, pendingLeaders ]
    in
    column [ width fill, spacing 10, height (fill |> maximum maxHeight), padding 10, clip ]
        [ wrappedRow [ Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 }, width fill, paddingXY 2 10, Font.center ]
            [ el [ alignLeft, fontNormal, Font.size 32, Font.extraBold ] <| text <| "Stake Leader Logs"
            , el [ fontNormal, Font.size 24, alignRight ] <|
                text <|
                    "Next block "
                        ++ (if time /= closestSchedule then
                                RelativeDateFormat.relativeTime time closestSchedule

                            else
                                "N/A"
                           )
            ]
        , if List.isEmpty pendingLeaders then
            Element.none

          else
            column
                [ width fill
                , spacing 10
                , Background.color (Element.rgb255 255 220 39)
                , height (fill |> maximum (maxHeight // nonEmptyLists))
                ]
                [ row [ width fill, paddingXY 2 10, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 } ]
                    [ el [ alignLeft, fontNormal, Font.bold, Font.size 28, padding 10 ] <| text <| "Pending"
                    ]
                , el [ scrollbarY, width fill ] <|
                    Element.indexedTable
                        [ fontMono
                        , Font.size 14
                        ]
                        { data = pendingLeadersSorted
                        , columns = leaderLogsColumns timeZone
                        }
                ]
        , if List.isEmpty createdLeaders then
            Element.none

          else
            column
                [ width fill
                , spacing 10
                , Background.color (Element.rgb255 114 202 151)
                , height (fill |> maximum (maxHeight // nonEmptyLists))
                ]
                [ row [ width fill, paddingXY 2 10, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 } ]
                    [ el [ alignLeft, fontNormal, Font.bold, Font.size 28, padding 10 ] <| text <| "Created"
                    ]
                , el [ scrollbarY, width fill ] <|
                    Element.indexedTable
                        [ fontMono
                        , Font.size 14
                        ]
                        { data = createdLeadersSorted
                        , columns = leaderLogsColumns timeZone
                        }
                ]
        , if List.isEmpty rejectedLeadersSorted then
            Element.none

          else
            column
                [ width fill
                , spacing 10
                , Background.color (Element.rgb255 255 102 26)
                , height (fill |> maximum (maxHeight // nonEmptyLists))
                ]
                [ row [ width fill, paddingXY 2 10, Border.widthEach { bottom = 1, left = 0, right = 0, top = 0 } ]
                    [ el [ alignLeft, fontNormal, Font.bold, Font.size 28, padding 10 ] <| text <| "Rejected"
                    ]
                , el [ scrollbarY, width fill ] <|
                    Element.indexedTable
                        [ fontMono
                        , Font.size 14
                        ]
                        { data = rejectedLeadersSorted
                        , columns = leaderLogsColumns timeZone
                        }
                ]
        ]



-- [ { header = Element.text "Enclave Leader ID"
--   , width = fill
--   , view =
--         \index leaderLog ->
--             row [ width fill ] [ text <| String.fromInt leaderLog.enclave_leader_id ]
--   }


rowColor : Int -> Attr decorative msg
rowColor index =
    if modBy 2 index == 0 then
        Background.color (Element.rgba255 0 0 0 0)

    else
        Background.color (Element.rgba255 0 0 0 0.1)


shrinkHash : String -> String
shrinkHash hash =
    String.left 4 hash ++ "..." ++ String.right 4 hash


leaderLogsColumns : Time.Zone -> List (IndexedColumn LeadersLogs Msg)
leaderLogsColumns timeZone =
    [ { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Created at"
      , width = fill
      , view =
            \index leaderLog ->
                row [ width fill, alignLeft, rowColor index, padding 10 ] [ text <| timeToDateString leaderLog.created_at_time timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Scheduled at Time"
      , width = fill
      , view =
            \index leaderLog ->
                row [ spacing 5, width fill, alignLeft, rowColor index, padding 10 ] [ text <| timeToDateString leaderLog.scheduled_at_time timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Scheduled at Date"
      , width = fill
      , view =
            \index leaderLog ->
                row [ spacing 5, width fill, alignLeft, rowColor index, padding 10 ] [ text <| leaderLog.scheduled_at_date ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Wake at"
      , width = fill
      , view =
            \index leaderLog ->
                row [ spacing 5, width fill, alignLeft, rowColor index, padding 10 ] [ text <| maybeTimeToDateString leaderLog.wake_at_time timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Finished at"
      , width = fill
      , view =
            \index leaderLog ->
                row [ spacing 5, width fill, alignLeft, rowColor index, padding 10 ] [ text <| maybeTimeToDateString leaderLog.finished_at_time timeZone ]
      }
    , { header = el [ fontNormal, Font.size 24, padding 10 ] <| Element.text "Status"
      , width = fill
      , view =
            \index leaderLog ->
                row [ spacing 5, width fill, alignLeft, rowColor index, padding 10 ]
                    [ case leaderLog.status of
                        Pending status ->
                            el [ alignLeft ] <| text <| status

                        Created createdBlock ->
                            viewLeaderLogsStatusCreatedBlock createdBlock.block

                        Rejected rejectedBlock ->
                            el [ alignLeft ] <| text <| rejectedBlock.rejected.reason
                    ]
      }
    ]


viewLeaderLogsStatusCreatedBlock : LeaderLogsCreatedBlock -> Element Msg
viewLeaderLogsStatusCreatedBlock leaderLogsStatusBlock =
    row [ spacing 10, width fill ]
        [ withTooltip "Block hash that has been created" <| el [ alignLeft ] <| text <| "Block"
        , el [ alignLeft ] <| text <| leaderLogsStatusBlock.block
        ]



-- , row [ width fill, spacing 10 ]
--     [ withTooltip "Chain length of created block" <| el [ alignLeft ] <| text <| "Chain length"
--     , el [ alignLeft ] <| text <| String.fromInt leaderLogsStatusBlock.chain_length
--     ]
-- viewFragmentLogs : List FragmentLogs -> Time.Zone -> List (Element Msg)
-- viewFragmentLogs fragmentLogsList timeZone =
--     List.map
--         (\fragmentLog ->
--             column []
--                 [ row []
--                     [ el [ alignLeft ] <| text <| "Fragment ID"
--                     , el [ alignRight ] <| text <| fragmentLog.fragment_id
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Received from"
--                     , el [ alignRight ] <| text <| fragmentLog.received_from
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Received at"
--                     , el [ alignRight ] <| text <| timeToDateString fragmentLog.received_at timeZone
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Last Updated at"
--                     , el [ alignRight ] <| text <| timeToDateString fragmentLog.last_updated_at timeZone
--                     ]
--                 , row []
--                     [ el [ alignLeft ] <| text <| "Status"
--                     , el [ alignRight ] <| text <| fragmentLog.status
--                     ]
--                 ]
--         )
--         fragmentLogsList
-- viewAccount : Account -> List (Element Msg)
-- viewAccount account =
--     [ row []
--         [ el [ alignLeft ] <| text <| "Value"
--         , el [ alignRight ] <| text <| String.fromInt account.value
--         ]
--     , row []
--         [ el [ alignLeft ] <| text <| "Delegation"
--         , viewPanel (viewStakePoolsSimple account.delegation.pools)
--         ]
--     , row []
--         [ el [ alignLeft ] <| text <| "Counter"
--         , el [ alignRight ] <| text <| String.fromInt account.counter
--         ]
--     ]


withTooltip : String -> Element msg -> Element msg
withTooltip str element =
    el
        [ Element.inFront <|
            el
                [ width fill
                , height fill
                , Element.transparent True
                , Element.mouseOver [ Element.transparent False ]
                , Element.onRight (tooltip str)
                ]
                Element.none
        ]
        element


tooltip : String -> Element msg
tooltip str =
    el
        [ Background.color (Element.rgb 0 0 0)
        , Font.color (Element.rgb 1 1 1)
        , Element.moveUp 32
        , Element.moveLeft 32
        , padding 4
        , Border.rounded 5
        , Font.size 14
        , Border.shadow
            { offset = ( 0, 3 ), blur = 6, size = 0, color = Element.rgba 0 0 0 0.32 }
        , Element.htmlAttribute (Html.Attributes.style "pointerEvents" "none")
        ]
        (text str)



-- Utils


formatAdaValue : Int -> String
formatAdaValue adaValue =
    let
        adaString =
            String.fromInt adaValue

        adaStringFormated =
            if String.length adaString > 6 then
                String.dropRight 6 adaString ++ "." ++ String.right 6 adaString

            else
                "0." ++ String.repeat (6 - String.length adaString) "0" ++ adaString

        adaFloat =
            Maybe.withDefault 0 (String.toFloat adaStringFormated)
    in
    FormatNumber.format { decimals = 6, thousandSeparator = ",", decimalSeparator = ".", negativePrefix = "−", negativeSuffix = "", positivePrefix = "", positiveSuffix = "", zeroPrefix = "", zeroSuffix = "" } adaFloat


maybeTimeToDateString : Maybe Posix -> Time.Zone -> String
maybeTimeToDateString decodedDate timeZone =
    case decodedDate of
        Just posixTime ->
            timeToDateString posixTime timeZone

        Nothing ->
            ""


timeToDateString : Posix -> Time.Zone -> String
timeToDateString posixTime timeZone =
    let
        toMonthNumberString monthType =
            case monthType of
                Time.Jan ->
                    "01"

                Time.Feb ->
                    "02"

                Time.Mar ->
                    "03"

                Time.Apr ->
                    "04"

                Time.May ->
                    "05"

                Time.Jun ->
                    "06"

                Time.Jul ->
                    "07"

                Time.Aug ->
                    "08"

                Time.Sep ->
                    "09"

                Time.Oct ->
                    "10"

                Time.Nov ->
                    "11"

                Time.Dec ->
                    "12"

        doubleDigits time =
            if String.length time < 2 then
                "0" ++ time

            else
                time

        year =
            String.fromInt (Time.toYear timeZone posixTime)

        month =
            toMonthNumberString (Time.toMonth timeZone posixTime)

        day =
            String.fromInt (Time.toDay timeZone posixTime)

        hour =
            doubleDigits <| String.fromInt (Time.toHour timeZone posixTime)

        minute =
            doubleDigits <| String.fromInt (Time.toMinute timeZone posixTime)

        second =
            doubleDigits <| String.fromInt (Time.toSecond timeZone posixTime)
    in
    month ++ "/" ++ day ++ "/" ++ year ++ " " ++ hour ++ ":" ++ minute ++ ":" ++ second



-- UI


mainBackgroundColor : Element.Color
mainBackgroundColor =
    Element.rgb255 239 239 239


sidebarBackgroundColor : Element.Color
sidebarBackgroundColor =
    Element.rgb255 70 80 88


fontNormal : Element.Attribute msg
fontNormal =
    Font.family
        [ Font.external
            { name = "Teko"
            , url = "https://fonts.googleapis.com/css?family=Teko"
            }
        , Font.sansSerif
        ]


fontMono : Element.Attribute msg
fontMono =
    Font.family
        [ Font.external
            { name = "Hack"
            , url = "https://cdnjs.cloudflare.com/ajax/libs/hack-font/3.003/web/hack.min.css"
            }
        , Font.monospace
        ]


blue : Element.Color
blue =
    Element.rgb255 41 106 157


toggleButtonStyle : List (Element.Attr () msg)
toggleButtonStyle =
    [ alignRight
    ]



-- SVG


menuSvg : Element msg
menuSvg =
    Element.html <| svg [ SvgA.width "40", SvgA.height "33", SvgA.viewBox "0 0 40 33" ] [ g [ SvgA.fill "none", SvgA.fillRule "evenodd" ] [ Svg.path [ SvgA.d "M-24-28h88v88h-88z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.d "M0 0h40v3H0V0zm0 15h40v3H0v-3zm0 15h40v3H0v-3z" ] [] ] ]


openMenuSvg : Element msg
openMenuSvg =
    Element.html <| svg [ SvgA.width "50", SvgA.height "33", SvgA.viewBox "0 0 50 33" ] [ g [ SvgA.fill "none", SvgA.fillRule "evenodd" ] [ Svg.path [ SvgA.d "M-19-28h88v88h-88z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.d "M0 0h40v3H0V0zm10 15h40v3H10v-3zM0 30h40v3H0v-3z" ] [] ] ]


aboutSvg : Element msg
aboutSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 28 28" ] [ Svg.path [ SvgA.style "fill: currentColor", SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M14 28C6.268 28 0 21.732 0 14S6.268 0 14 0s14 6.268 14 14-6.268 14-14 14zM8.742 11.01h3.174c.03-.947.674-1.572 1.611-1.572.918 0 1.563.546 1.563 1.337 0 .86-.342 1.27-1.67 2.051-1.357.781-1.846 1.66-1.66 3.33l.03.39h3.046v-.458c0-.918.371-1.358 1.709-2.129 1.435-.83 2.11-1.826 2.11-3.271 0-2.344-1.934-3.907-4.903-3.907-3.125 0-4.98 1.67-5.01 4.229zM13.4 21.342c1.26 0 1.993-.654 1.993-1.778 0-1.132-.733-1.787-1.993-1.787-1.26 0-2.002.655-2.002 1.787 0 1.123.743 1.778 2.002 1.778z" ] [] ]


adaSvg : Element msg
adaSvg =
    Element.html <| svg [ SvgA.width "12", SvgA.height "12", SvgA.viewBox "0 0 18 20", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd", SvgA.strokeLinecap "round" ] [ g [ SvgA.id "new_wallet_screen_summary", SvgA.transform "translate(-1238.000000, -715.000000)", SvgA.stroke "#000", SvgA.strokeWidth "1.5" ] [ g [ SvgA.id "amount-copy", SvgA.transform "translate(760.000000, 460.000000)" ] [ g [ SvgA.id "pending", SvgA.transform "translate(60.000000, 246.000000)" ] [ g [ SvgA.id "ada-symbol-smallest-dark-copy", SvgA.transform "translate(419.000000, 10.000000)" ] [ polyline [ SvgA.id "Path-2", SvgA.strokeLinejoin "round", SvgA.points "0.505531915 18 7.92 0 15.3344681 18" ] [], Svg.path [ SvgA.d "M1.68510638,7.75 L14.1548936,7.75", SvgA.id "Line" ] [], Svg.path [ SvgA.d "M0,10.75 L15.84,10.75", SvgA.id "Line-Copy" ] [] ] ] ] ] ] ]


sortAscSvg : Element msg
sortAscSvg =
    Element.html <| svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 14 15", SvgA.version "1.1", SvgA.id "svg4" ] [ Svg.path [ SvgA.d "M 0,15 H 14 V 12 H 0 Z M 0,9 H 10 V 6 H 0 Z M 0,3 H 6 V 0 H 0 Z", SvgA.fillRule "evenodd", SvgA.fill "#5E6066" ] [] ]


sortDscSvg : Element msg
sortDscSvg =
    Element.html <| svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 14 15" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M0 0h14v3H0V0zm0 6h10v3H0V6zm0 6h6v3H0v-3z" ] [] ]


erroredSvg : Element msg
erroredSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 24 24" ] [ Svg.path [ SvgA.fill "#EA4C5B", SvgA.d "M12 24C5.373 24 0 18.627 0 12S5.373 0 12 0s12 5.373 12 12-5.373 12-12 12zM10.202 4.499l.28 10.398h2.868L13.64 4.5h-3.438zm-.236 13.997c0 1.128.752 1.826 1.955 1.826 1.214 0 1.955-.698 1.955-1.826 0-1.139-.741-1.837-1.955-1.837-1.203 0-1.955.698-1.955 1.837z" ] [] ]


fetchDataSvg : Element msg
fetchDataSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 130 116", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "3", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "fetch_data", SvgA.transform "translate(-1885.000000, -1022.000000)" ] [ g [ SvgA.id "dialog", SvgA.transform "translate(1000.000000, 355.000000)" ] [ g [ SvgA.id "fetch", SvgA.transform "translate(660.000000, 570.000000)" ] [ g [ SvgA.id "fetch-ic", SvgA.transform "translate(196.000000, 61.000000)" ] [ circle [ SvgA.id "Oval", SvgA.cx "94", SvgA.cy "94", SvgA.r "94" ] [], Svg.path [ SvgA.d "M132,68 C130.341,68 129,69.341 129,71 C129,72.659 130.341,74 132,74 L144,74 C148.968,74 153,78.026 153,83 L153,137 C153,141.968 148.968,146 144,146 L44,146 C39.032,146 35,141.968 35,137 L35,83 C35,78.026 39.032,74 44,74 L56,74 C57.659,74 59,72.659 59,71 C59,69.341 57.659,68 56,68 L44,68 C35.714,68 29,74.714 29,83 L29,137 C29,145.286 35.714,152 44,152 L144,152 C152.286,152 159,145.286 159,137 L159,83 C159,74.714 152.286,68 144,68 L132,68 Z M91,39 L91,115.857 L71.998,96.855 C70.855,95.712 68.998,95.712 67.855,96.855 C66.712,98.004 66.712,99.855 67.855,100.998 L91.579,124.728 C91.678,124.863 91.732,125.022 91.855,125.145 C92.437,125.73 93.205,126.006 93.97,125.994 C94.792,126.006 95.56,125.73 96.145,125.145 C96.271,125.016 96.331,124.857 96.43,124.716 L120.145,100.998 C121.288,99.855 121.288,98.004 120.145,96.855 C118.999,95.712 117.145,95.712 116.002,96.855 L97,115.857 L97,39 C97,37.344 95.659,36 94,36 C92.341,36 91,37.341 91,39 Z", SvgA.id "fetch", SvgA.fill "#5E6066", SvgA.style "fill: currentColor" ] [] ] ] ] ] ] ]


hideSvg : Element msg
hideSvg =
    Element.html <| svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 38 38" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M11.708 27.555l-6.143 6.143-1.414-1.414 5.572-5.572C6.29 25.055 3.049 22.434 0 18.85c5.671-6.666 12.004-10 19-10 2.516 0 4.947.432 7.292 1.294L32.435 4l1.414 1.414-5.572 5.572c3.433 1.657 6.674 4.278 9.723 7.863-5.671 6.667-12.004 10-19 10-2.516 0-4.947-.431-7.292-1.294zm1.512-1.511c1.867.537 3.794.805 5.78.805 6.26 0 11.926-2.666 17-8-2.853-2.998-5.892-5.153-9.12-6.466l-3.708 3.709a5 5 0 0 1-6.929 6.929l-3.023 3.023zm-2.1-.728l3.708-3.71a5 5 0 0 1 6.929-6.929l3.023-3.022a20.802 20.802 0 0 0-5.78-.806c-6.26 0-11.926 2.667-17 8 2.853 2.998 5.892 5.154 9.12 6.467z" ] [] ]


loadedSvg : Element msg
loadedSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 220 220", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "wallet_add_wallet_notification_big", SvgA.transform "translate(-1526.000000, -734.000000)" ] [ g [ SvgA.id "notification", SvgA.transform "translate(720.000000, 168.000000)" ] [ g [ SvgA.id "description", SvgA.transform "translate(660.000000, 566.000000)" ] [ g [ SvgA.id "checked-light", SvgA.transform "translate(146.000000, 0.000000)" ] [ Svg.path [ SvgA.d "M110,220 C49.2486775,220 0,170.751322 0,110 C0,49.2486775 49.2486775,0 110,0 C170.751322,0 220,49.2486775 220,110 C220,170.751322 170.751322,220 110,220 Z M110,205 C162.467051,205 205,162.467051 205,110 C205,57.5329488 162.467051,15 110,15 C57.5329488,15 15,57.5329488 15,110 C15,162.467051 57.5329488,205 110,205 Z", SvgA.id "Combined-Shape", SvgA.fill "#05f079" ] [], Svg.path [ SvgA.d "M90,110 L145,110 L145,125 L75,125 L75,117.5 L75,85 L90,85 L90,110 Z", SvgA.id "Combined-Shape", SvgA.fill "#05f079", SvgA.transform "translate(110.000000, 105.000000) rotate(-45.000000) translate(-110.000000, -105.000000) " ] [] ] ] ] ] ] ]


leadersSvg : Element msg
leadersSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 24 24" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M11.219,3.375L8,7.399L4.781,3.375C4.515,3.043,4.068,2.916,3.669,3.056C3.269,3.197,3,3.575,3,4v15c0,1.103,0.897,2,2,2 h14c1.103,0,2-0.897,2-2V4c0-0.425-0.269-0.803-0.669-0.944c-0.4-0.138-0.846-0.012-1.112,0.319L16,7.399l-3.219-4.024 C12.4,2.901,11.6,2.901,11.219,3.375z M5,19v-2h14.001v2H5z M15.219,9.625c0.381,0.475,1.182,0.475,1.563,0L19,6.851L19.001,15H5 V6.851l2.219,2.774c0.381,0.475,1.182,0.475,1.563,0L12,5.601L15.219,9.625z" ] [] ]


loadingSvg : Element msg
loadingSvg =
    Element.html <| svg [ SvgA.width "122", SvgA.height "122", SvgA.viewBox "0 0 122 122", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "styles", SvgA.transform "translate(-860.000000, -5380.000000)", SvgA.fill "#465058" ] [ g [ SvgA.id "spinner-light", SvgA.transform "translate(860.000000, 5380.000000)" ] [ Svg.path [ SvgA.d "M18.5735931,18.5735931 C-4.85786437,42.0050506 -4.85786437,79.9949494 18.5735931,103.426407 C42.0050506,126.857864 79.9949494,126.857864 103.426407,103.426407 L97.7695526,97.7695526 C77.4622895,118.076816 44.5377105,118.076816 24.2304474,97.7695526 C3.92318421,77.4622895 3.92318421,44.5377105 24.2304474,24.2304474 L18.5735931,18.5735931 L18.5735931,18.5735931 L18.5735931,18.5735931 Z", SvgA.id "spinner" ] [] ] ] ] ]


fetchingSvg : Element msg
fetchingSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 122 122", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "styles", SvgA.transform "translate(-860.000000, -5380.000000)", SvgA.fill "#FAFBFC" ] [ g [ SvgA.id "spinner-light", SvgA.transform "translate(860.000000, 5380.000000)" ] [ Svg.path [ SvgA.d "M18.5735931,18.5735931 C-4.85786437,42.0050506 -4.85786437,79.9949494 18.5735931,103.426407 C42.0050506,126.857864 79.9949494,126.857864 103.426407,103.426407 L97.7695526,97.7695526 C77.4622895,118.076816 44.5377105,118.076816 24.2304474,97.7695526 C3.92318421,77.4622895 3.92318421,44.5377105 24.2304474,24.2304474 L18.5735931,18.5735931 L18.5735931,18.5735931 L18.5735931,18.5735931 Z", SvgA.id "spinner" ] [] ] ] ] ]


monitorSvg : Element msg
monitorSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 44 44" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.fillRule "evenodd", SvgA.d "M38.737 13C35.53 7.046 29.237 3 22 3 11.507 3 3 11.507 3 22H0C0 9.85 9.85 0 22 0c8.103 0 15.182 4.38 19 10.902V7h3v9h-9v-3h3.737zM3.967 28H9v3H5.263C8.47 36.954 14.763 41 22 41c10.493 0 19-8.507 19-19h3c0 12.15-9.85 22-22 22-8.103 0-15.182-4.38-19-10.902V37H0v-9H3.967z" ] [] ]


nodeSvg : Element msg
nodeSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 62 58" ] [ g [ SvgA.fill "none", SvgA.fillRule "nonzero" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M10.75 0C16.687 0 21.5 4.813 21.5 10.75S16.687 21.5 10.75 21.5 0 16.687 0 10.75 4.813 0 10.75 0zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM10.75 36c5.937 0 10.75 4.813 10.75 10.75S16.687 57.5 10.75 57.5 0 52.687 0 46.75 4.813 36 10.75 36zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM50.75 0C56.687 0 61.5 4.813 61.5 10.75S56.687 21.5 50.75 21.5 40 16.687 40 10.75 44.813 0 50.75 0zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5zM50.75 36c5.937 0 10.75 4.813 10.75 10.75S56.687 57.5 50.75 57.5 40 52.687 40 46.75 44.813 36 50.75 36zm0 2.5a8.25 8.25 0 1 0 0 16.5 8.25 8.25 0 0 0 0-16.5z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M30.75 0C46.628 0 59.5 12.872 59.5 28.75S46.628 57.5 30.75 57.5 2 44.628 2 28.75 14.872 0 30.75 0zm0 2.5C16.253 2.5 4.5 14.253 4.5 28.75S16.253 55 30.75 55 57 43.247 57 28.75 45.247 2.5 30.75 2.5z" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M10.75 36c2.969 0 5.657 1.204 7.602 3.15l-1.767 1.767A8.25 8.25 0 1 0 4.918 52.584L3.15 54.353A10.716 10.716 0 0 1 0 46.75C0 40.813 4.813 36 10.75 36zM10.75 0C16.687 0 21.5 4.813 21.5 10.75c0 2.969-1.203 5.656-3.149 7.601l-1.767-1.767A8.25 8.25 0 1 0 4.917 4.917L3.148 3.148A10.716 10.716 0 0 1 10.75 0zM61.5 10.75c0 5.937-4.813 10.75-10.75 10.75a10.716 10.716 0 0 1-7.601-3.149l1.768-1.766A8.25 8.25 0 0 0 56.584 4.918l1.767-1.77A10.716 10.716 0 0 1 61.5 10.75zM43.149 39.149l1.767 1.767a8.25 8.25 0 1 0 11.667 11.667l1.768 1.768A10.716 10.716 0 0 1 50.75 57.5C44.813 57.5 40 52.687 40 46.75c0-2.969 1.203-5.656 3.149-7.601z" ] [] ] ]


settingsSvg : Element msg
settingsSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 42 44", SvgA.style "fill: currentColor" ] [ g [ SvgA.fill "none", SvgA.fillRule "evenodd", SvgA.transform "translate(-23 -22)" ] [ circle [ SvgA.cx "44", SvgA.cy "44", SvgA.r "44" ] [], Svg.path [ SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.d "M55 46H33c-5.521 0-10 4.477-10 10 0 5.521 4.479 10 10 10h22c5.523 0 10-4.479 10-10 0-5.524-4.477-10-10-10zm0 18H33a8 8 0 0 1 0-16h22a8 8 0 0 1 0 16zm0-42H33c-5.521 0-10 4.477-10 10 0 5.521 4.479 10 10 10h22c5.523 0 10-4.479 10-10 0-5.524-4.477-10-10-10zm0 18H33a8 8 0 0 1 0-16h22a8 8 0 0 1 0 16zm0 10a6 6 0 0 0 0 12 6 6 0 0 0 0-12zm0 10a4 4 0 1 1 0-8 4 4 0 0 1 0 8zM33 26a6 6 0 0 0 0 12 6 6 0 0 0 0-12zm0 10a4 4 0 1 1 0-8 4 4 0 0 1 0 8z" ] [] ] ]


showSvg : Element msg
showSvg =
    Element.html <| svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 38 38" ] [ Svg.path [ SvgA.fill "#5E6066", SvgA.fillRule "evenodd", SvgA.d "M19 29c-6.996 0-13.329-3.333-19-10C5.671 12.333 12.004 9 19 9s13.329 3.333 19 10c-5.671 6.667-12.004 10-19 10zm0-2c6.26 0 11.926-2.667 17-8-5.074-5.333-10.74-8-17-8S7.074 13.667 2 19c5.074 5.333 10.74 8 17 8zm0-3a5 5 0 1 1 0-10 5 5 0 0 1 0 10z" ] [] ]


stakePoolsSvg : Element msg
stakePoolsSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 44 36", SvgA.version "1.1" ] [ g [ SvgA.id "Symbols", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "small-menu-staking-selected", SvgA.transform "translate(-62.000000, -906.000000)" ] [ g [ SvgA.id "menu" ] [ g [ SvgA.id "main-menu", SvgA.transform "translate(40.000000, 208.000000)" ] [ g [ SvgA.id "staking-ic", SvgA.transform "translate(0.000000, 672.000000)" ] [ circle [ SvgA.id "Oval", SvgA.cx "44", SvgA.cy "44", SvgA.r "44" ] [], polygon [ SvgA.id "Fill-164", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "34 60 34 40 28 40 28 60 26 60 26 38 36 38 36 60" ] [], polygon [ SvgA.id "Fill-165", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "47 60 47 34 41 34 41 60 39 60 39 32 49 32 49 60" ] [], polygon [ SvgA.id "Fill-166", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "60 60 60 28 54 28 54 60 52 60 52 26 62 26 62 60" ] [], polygon [ SvgA.id "Fill-167", SvgA.fill "#5E6066", SvgA.style "fill: currentColor", SvgA.points "22 60 66 60 66 62 22 62" ] [] ] ] ] ] ] ]


statusSvg : Element msg
statusSvg =
    Element.html <| svg [ SvgA.width "30", SvgA.height "30", SvgA.viewBox "0 0 30 30" ] [ g [ SvgA.fill "#EE2257", SvgA.fillRule "nonzero" ] [ Svg.path [ SvgA.d "M19.353 25.044a.937.937 0 0 1-1.828-.076L14.711 9.423 11.7 22.584a.937.937 0 0 1-1.817.042l-1.986-7.13-.18.54a.938.938 0 0 1-.89.642H.094C.928 24.172 7.283 30 15 30c7.717 0 14.072-5.828 14.906-13.322h-8.312l-2.24 8.366z" ] [], Svg.path [ SvgA.d "M7.087 11.99a.938.938 0 0 1 1.793.044l1.817 6.526 3.207-14.017a.937.937 0 0 1 1.837.042l2.884 15.931 1.344-5.018a.937.937 0 0 1 .906-.695h9.123C29.892 6.61 23.218 0 15 0S.108 6.61.002 14.803h6.149l.936-2.813z" ] [] ] ]


anchorSvg : Element msg
anchorSvg =
    Element.html <| svg [ SvgA.width "18", SvgA.height "18", SvgA.viewBox "0 0 23 14", SvgA.version "1.1" ] [ g [ SvgA.id "Page-1", SvgA.stroke "none", SvgA.strokeWidth "1", SvgA.fill "none", SvgA.fillRule "evenodd" ] [ g [ SvgA.id "wallet_add_wallet_create_personal", SvgA.transform "translate(-2157.000000, -916.000000)", SvgA.fill "#5E6066" ] [ g [ SvgA.id "dialog", SvgA.transform "translate(1000.000000, 331.000000)" ] [ g [ SvgA.id "currency", SvgA.transform "translate(60.000000, 478.000000)" ] [ g [ SvgA.id "input", SvgA.transform "translate(0.000000, 64.000000)" ] [ polygon [ SvgA.id "expand-arrow", SvgA.points "1108.5 51.4 1099.875 43 1097 45.8 1107.0625 55.6 1108.5 57 1120 45.8 1117.125 43" ] [] ] ] ] ] ] ]


oxheadSvg : Element msg
oxheadSvg =
    Element.html <|
        svg
            [ SvgA.width "40", SvgA.height "40", SvgA.viewBox "0 0 324 323", SvgA.fill "none", SvgA.xmlSpace "http://www.w3.org/2000/svg" ]
            [ g
                [ SvgA.clipPath "url(#clip0)" ]
                [ Svg.path
                    [ SvgA.d "M84 224L154 153.42V-7.50698e-06L84 70.5811L84 224Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M169 1.53035e-05L239 70.5801V224L169 153.419V1.53035e-05Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M237.735 241.368L161.368 165L85 241.368L161.368 317.735L237.735 241.368Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M245.999 253L315 323H177L245.999 253Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M253 241L323 172V310L253 241Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M77.0005 253L8.00001 323H146L77.0005 253Z", SvgA.style "fill: currentColor" ]
                    []
                , Svg.path
                    [ SvgA.d "M70 241L0 172L0 310L70 241Z", SvgA.style "fill: currentColor" ]
                    []
                ]
            , defs
                []
                [ Svg.clipPath
                    [ SvgA.id "clip0" ]
                    [ rect
                        [ SvgA.width "323.141", SvgA.height "322.95", SvgA.style "fill: currentColor" ]
                        []
                    ]
                ]
            ]


oxheadSvgColored : Element msg
oxheadSvgColored =
    Element.html <|
        svg
            [ SvgA.width "180", SvgA.height "180", SvgA.viewBox "0 0 324 323", SvgA.fill "none", SvgA.xmlSpace "http://www.w3.org/2000/svg" ]
            [ g
                [ SvgA.clipPath "url(#clip0)" ]
                [ Svg.path
                    [ SvgA.d "M84 224L154 153.42V-7.50698e-06L84 70.5811L84 224Z", SvgA.fill "#7FD13B" ]
                    []
                , Svg.path
                    [ SvgA.d "M169 1.53035e-05L239 70.5801V224L169 153.419V1.53035e-05Z", SvgA.fill "white" ]
                    []
                , Svg.path
                    [ SvgA.d "M237.735 241.368L161.368 165L85 241.368L161.368 317.735L237.735 241.368Z", SvgA.fill "#5A6378" ]
                    []
                , Svg.path
                    [ SvgA.d "M245.999 253L315 323H177L245.999 253Z", SvgA.fill "#60B5CC" ]
                    []
                , Svg.path
                    [ SvgA.d "M253 241L323 172V310L253 241Z", SvgA.fill "#F0AD00" ]
                    []
                , Svg.path
                    [ SvgA.d "M77.0005 253L8.00001 323H146L77.0005 253Z", SvgA.fill "#F0AD00" ]
                    []
                , Svg.path
                    [ SvgA.d "M70 241L0 172L0 310L70 241Z", SvgA.fill "#60B5CC" ]
                    []
                ]
            , defs
                []
                [ Svg.clipPath
                    [ SvgA.id "clip0" ]
                    [ rect
                        [ SvgA.width "323.141", SvgA.height "322.95", SvgA.fill "white" ]
                        []
                    ]
                ]
            ]


elmLogoSvg : Element msg
elmLogoSvg =
    Element.html <|
        svg
            [ SvgA.version "1.1"
            , SvgA.x "0"
            , SvgA.y "0"
            , SvgA.viewBox "0 0 323.141 322.95"
            , SvgA.width "40"
            , SvgA.height "40"
            ]
            [ polygon [ SvgA.fill "#F0AD00", SvgA.points "161.649,152.782 231.514,82.916 91.783,82.916" ] []
            , polygon [ SvgA.fill "#7FD13B", SvgA.points "8.867,0 79.241,70.375 232.213,70.375 161.838,0" ] []
            , rect
                [ SvgA.fill "#7FD13B"
                , SvgA.x "192.99"
                , SvgA.y "107.392"
                , SvgA.width "107.676"
                , SvgA.height "108.167"
                , SvgA.transform "matrix(0.7071 0.7071 -0.7071 0.7071 186.4727 -127.2386)"
                ]
                []
            , polygon [ SvgA.fill "#60B5CC", SvgA.points "323.298,143.724 323.298,0 179.573,0" ] []
            , polygon [ SvgA.fill "#5A6378", SvgA.points "152.781,161.649 0,8.868 0,314.432" ] []
            , polygon [ SvgA.fill "#F0AD00", SvgA.points "255.522,246.655 323.298,314.432 323.298,178.879" ] []
            , polygon [ SvgA.fill "#60B5CC", SvgA.points "161.649,170.517 8.869,323.298 314.43,323.298" ] []
            ]


gitlabLogoSvg : Element msg
gitlabLogoSvg =
    Element.html <| svg [ SvgA.width "50", SvgA.height "45", SvgA.viewBox "0 0 492.50943 453.67966" ] [ g [ SvgA.fill "none", SvgA.fillRule "evenodd" ] [ Svg.path [ SvgA.d "M491.58891 259.39833l-27.55867-84.81467L409.41291 6.48633c-2.80934-8.648-15.04533-8.648-17.856 0l-54.61867 168.09733H155.57158l-54.62-168.09733c-2.80933-8.648-15.04533-8.648-17.856 0L28.47825 174.58366.92092 259.39833c-2.514669 7.736.24 16.21066 6.82 20.992l238.51333 173.28933 238.51466-173.28933c6.58-4.78134 9.33333-13.256 6.82-20.992", SvgA.fill "#fc6d26" ] [], Svg.path [ SvgA.d "M246.25478 453.67966l90.684-279.096h-181.368z", SvgA.fill "#e24329" ] [], Svg.path [ SvgA.d "M246.25478 453.67912l-90.684-279.09466h-127.092z", SvgA.fill "#fc6d26" ] [], Svg.path [ SvgA.d "M28.47878 174.58406L.92012 259.39873c-2.513336 7.736.24 16.21066 6.82133 20.99066l238.51333 173.28933z", SvgA.fill "#fca326" ] [], Svg.path [ SvgA.d "M28.47878 174.58433h127.092L100.95212 6.487c-2.81067-8.64933-15.04667-8.64933-17.856 0z", SvgA.fill "#e24329" ] [], Svg.path [ SvgA.d "M246.25478 453.67912l90.684-279.09466h127.09199z", SvgA.fill "#fc6d26" ] [], Svg.path [ SvgA.d "M464.03064 174.58406l27.55867 84.81467c2.51333 7.736-.24 16.21066-6.82134 20.99066L246.25465 453.67872z", SvgA.fill "#fca326" ] [], Svg.path [ SvgA.d "M464.03064 174.58433h-127.092L391.55731 6.487c2.81066-8.64933 15.04666-8.64933 17.856 0z", SvgA.fill "#e24329" ] [] ] ]



-- Main


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }
