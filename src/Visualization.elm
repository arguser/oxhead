module Visualization exposing (viewEpoch)

import Array exposing (Array)
import Color exposing (Color)
import Path
import Shape exposing (defaultPieConfig)
import TypedSvg exposing (g, svg, text_)
import TypedSvg.Attributes exposing (dy, fill, fontWeight, stroke, textAnchor, transform)
import TypedSvg.Attributes.InPx exposing (fontSize, height, width)
import TypedSvg.Core exposing (Svg, text)
import TypedSvg.Types exposing (AnchorAlignment(..), FontWeight(..), Paint(..), Transform(..), em)


w : Float
w =
    495


h : Float
h =
    252


colors : Array Color
colors =
    Array.fromList
        [ Color.rgb255 152 171 198
        , Color.rgba 152 171 198 255
        ]


radius : Float
radius =
    min w h / 2


type alias ChartConfig =
    { outerRadius : Float
    , innerRadius : Float
    , padAngle : Float
    , cornerRadius : Float
    , labelPosition : Float
    }


type alias Model =
    { config : ChartConfig
    , data : List ( String, Float )
    }


drawChart : ChartConfig -> List ( String, Float ) -> Svg msg
drawChart config model =
    let
        pieData =
            model
                |> List.map Tuple.second
                |> Shape.pie
                    { defaultPieConfig
                        | innerRadius = config.innerRadius
                        , outerRadius = config.outerRadius
                        , padAngle = config.padAngle
                        , cornerRadius = config.cornerRadius
                        , sortingFn = \_ _ -> EQ
                    }

        makeSlice index datum =
            Path.element (Shape.arc datum) [ fill <| Paint <| Maybe.withDefault Color.black <| Array.get index colors, stroke <| Paint Color.white ]

        makeLabel slice ( label, value ) =
            let
                ( x, y ) =
                    Shape.centroid { slice | innerRadius = config.labelPosition, outerRadius = config.labelPosition }
            in
            text_
                [ transform [ Translate x y ]
                , dy (em 0.35)
                , textAnchor AnchorMiddle
                , fontSize 24
                , fontWeight FontWeightBold
                ]
                [ text label ]
    in
    svg [ width (radius * 2), height (radius * 2) ]
        [ g [ transform [ Translate radius radius ] ]
            [ g [] <| List.indexedMap makeSlice pieData
            , g [] <| List.map2 makeLabel pieData model
            ]
        ]


viewEpoch : Float -> Float -> Svg msg
viewEpoch currentSlot slotsPerEpoch =
    let
        initModel =
            init currentSlot slotsPerEpoch
    in
    drawChart initModel.config initModel.data


data : Float -> Float -> List ( String, Float )
data currentSlot totalSlots =
    let
        slotLefts =
            totalSlots - currentSlot

        completion =
            round <| (currentSlot * 100) / totalSlots
    in
    [ ( String.fromInt completion ++ "%", currentSlot )
    , ( "", slotLefts )
    ]


init : Float -> Float -> { config : ChartConfig, data : List ( String, Float ) }
init currentSlot slotsPerEpoch =
    { config =
        { outerRadius = 110
        , innerRadius = 100
        , padAngle = 0.02
        , cornerRadius = 20
        , labelPosition = 0
        }
    , data = data currentSlot slotsPerEpoch
    }
